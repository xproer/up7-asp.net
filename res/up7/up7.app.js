function Uploader7App(mgr) {
    var _this = this;
    this.ins = mgr;
    this.edgeApp = mgr.edgeApp;
    this.Config = mgr.Config;
    this.checkFF = function () {
        var mimetype = navigator.mimeTypes;
        if (typeof mimetype == "object" && mimetype.length) {
            for (var i = 0; i < mimetype.length; i++) {
                var enabled = mimetype[i].type == this.Config.firefox.type;
                if (!enabled) enabled = mimetype[i].type == this.Config.firefox.type.toLowerCase();
                if (enabled) return mimetype[i].enabledPlugin;
            }
        }
        else {
            mimetype = [this.Config.firefox.type];
        }
        if (mimetype) {
            return mimetype.enabledPlugin;
        }
        return false;
    };
    this.Setup = function () {
        //文件夹选择控件
        acx += '<object id="objHttpPartition" classid="clsid:' + this.Config.ie.part.clsid + '"';
        acx += ' codebase="' + this.Config.ie.path + '" width="1" height="1" ></object>';

        $("body").append(acx);
    };
    this.init = function () {
        var param = $.extend({ config: this.Config },{ name: "init" });
        this.postMessage(param);
    };
    this.initNat = function () {
        if (!this.chrome45) return;
        this.exitEvent();
        document.addEventListener('Uploader6EventCallBack', function (evt) {
            this.recvMessage(JSON.stringify(evt.detail));
        });
    };
    this.initEdge = function () {
        this.edgeApp.run();
    };
    this.exit = function () {
        var par = { name: 'exit' };
        this.postMessage(par);
    };
    this.exitEvent = function () {
        var obj = this;
        $(window).bind("beforeunload", function () { obj.exit(); });
    };
    this.addFile = function (json) {
        var param = $.extend({}, json, { name: "add_file" });
        this.postMessage(param);
    };
    this.addFolder = function (json) {
        var param = $.extend({}, json, { name: "add_folder" });
        this.postMessage(param);
    };
    this.openFiles = function () {
        var param = { name: "open_files"};
        this.postMessage(param);
    };
    this.openFolders = function () {
        var param = { name: "open_folders"};
        this.postMessage(param);
    };
    this.pasteFiles = function () {
        var param = { name: "paste_files"};
        this.postMessage(param);
    };
    this.checkFile = function (f) {
        var param = $.extend({}, f, { name: "check_file" });
        this.postMessage(param);
    };
    this.checkFolder = function (fd) {
        var param = $.extend({}, fd, { name: "check_folder" });
        this.postMessage(param);
    };
    this.scanFolder = function (fd) {
        var param = $.extend({}, fd, { name: "scan_folder" });
        this.postMessage(param);
    };
    this.checkFolderNat = function (fd) {
        var param = { name: "check_folder", folder: JSON.stringify(fd) };
        this.postMessage(param);
    };
    this.postFile = function (f) {
        var param = $.extend({}, f, { name: "post_file" });
        this.postMessage(param);
    };
    this.postFolder = function (fd) {
        var param = $.extend({}, fd, { name: "post_folder" });
        this.postMessage(param);
    };
    this.updateFolder = function (fd) {
        var param = $.extend({}, fd, { name: "update_folder" });
        this.postMessage(param);
    };
    this.delFolder = function (v) {
        var param = $.extend({}, v, { name: "del_folder" });
        this.postMessage(param);
    };
    this.stopFile = function (f) {
        var param = $.extend({}, f, { name: "stop_file" });
        this.postMessage(param);
    };
    this.delFile = function (f) {
        var param = { name: "del_file", id: f.id };
        this.postMessage(param);
    };
    this.stopQueue = function () {
        var param = { name: "stop_queue" };
        this.postMessage(param);
    };
    this.postMessage = function (json) {
        try {
            this.ins.parter.postMessage(JSON.stringify(json));
        } catch (e) { alert(e); }
    };
    this.postMessageNat = function (par) {
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(this.entID, true, false, par);
        document.dispatchEvent(evt);
    };
    this.postMessageEdge = function (par) {
        this.edgeApp.send(par);
    };
}