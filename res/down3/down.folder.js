﻿function FdDownloader(fileLoc, mgr)
{
    var _this = this;
    this.ui = { 
        ico:{file:null,fd:null,err:null,inf:null},
        msg: null, process: null, percent: null, 
        errPnl:null,
        errFiles:null,
        btn: {del:null,cancel:null,down:null,stop:null,errInf:null},
        div:null,split:null};
    this.app = mgr.app;
    this.Manager = mgr;
    this.Config = mgr.Config;
    this.fields = $.extend({},mgr.Config.Fields);//每一个对象自带一个fields幅本
    this.State = this.Config.state.None;
    this.event = mgr.event;
    this.fileSvr = {
          id:""
        , f_id: ""
        , uid: 0
        , nameLoc: ""//自定义文件名称
        , folderLoc: this.Config["Folder"]
        , pathLoc: ""
        , fileUrl: this.Config["UrlDown"]
        , lenLoc: 0
        , perLoc: "0%"
        , lenSvr: 0
        , sizeSvr:"0byte"
        , complete: false
        , errors: 0
        , success:0
        , fdTask: true
        , files: null
        , svrInit: false
    };
    $.extend(this.fileSvr, fileLoc);//覆盖配置
    $.extend(this.fileSvr, { fields: this.fields });
    var url = this.Config["UrlDown"] + "?" + this.Manager.to_params(this.fields);
    $.extend(this.fileSvr, fileLoc, { fileUrl: url });//覆盖配置

    this.hideBtns = function ()
    {
        $.each(this.ui.btn, function (i, n)
        {
            $(n).hide();
        });
    };

    //方法-准备
    this.ready = function ()
    {
        this.hideBtns();
        this.ui.btn.del.click(function () { _this.remove(); });
        this.ui.btn.stop.click(function () { _this.stop(); });
        this.ui.btn.down.click(function () { _this.Manager.allStoped = false; _this.down(); });
        this.ui.btn.cancel.click(function () { _this.remove(); });
        this.ui.btn.open.click(function () { _this.openPath(); });
        this.ui.btn.errInf.click(function(){_this.ui.errFiles.toggleClass("d-hide");});

        this.ui.btn.down.show();
        this.ui.btn.cancel.show();
        this.ui.msg.text("正在下载队列中等待...");
        this.ui.ico.file.hide();
        this.ui.ico.fd.show();
        this.State = this.Config.state.Ready;
        this.Manager.add_wait(this.fileSvr.id);//添加到等待队列
    };
    //自定义配置,
    this.reset_fields = function (v) {
        if (v == null) return;
        $.extend(this.fields, v);
        //单独拼接url
        var url = this.Config["UrlDown"] + "?" + this.Manager.to_params(this.fields);
        $.extend(this.fileSvr, { fileUrl: url });//覆盖配置
    };

    this.addQueue = function ()
    {
        this.app.addFolder(this.fileSvr);
    };

    //方法-开始下载
    this.down = function ()
    {
        if (this.fileSvr.svrInit) {
            this.hideBtns();
            this.ui.btn.stop.show();
            this.ui.btn.errInf.hide();
            this.ui.errFiles.html("");
            this.ui.msg.text("开始连接服务器...");
            this.State = this.Config.state.Posting;
            this.Manager.del_wait(this.fileSvr.id);
            this.app.downFolder(this.fileSvr);//下载队列
        }
        else { this.svr_create(); }
    };

    //方法-停止传输
    this.stop = function ()
    {
        this.hideBtns();
        this.State = this.Config.state.Stop;
        this.ui.msg.text("下载已停止");
        this.app.stopFile(this.fileSvr);
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
    };

    this.remove = function ()
    {
        this.app.stopFile(this.fileSvr);
        //从上传列表中删除
        this.ui.split.remove();
        this.ui.div.remove();
        this.Manager.remove_wait(this.fileSvr.fileUrl);
        this.Manager.remove_url(this.fileSvr.nameLoc);
        this.svr_delete();
    };

    this.openPath = function ()
    {
        this.app.openPath(this.fileSvr);
    };
    this.init_complete = function (json) {
        $.extend(this.fileSvr, json);
        if (!this.fileSvr.svrInit) this.svr_create();//
    };

    //在出错，停止中调用
    this.svr_update = function (json)
    {
        var param = $.extend({}, this.fields, this.fileSvr, { time: new Date().getTime() });
        $.extend(param, { perLoc: encodeURIComponent(this.fileSvr.perLoc), sizeLoc: encodeURIComponent(this.fileSvr.sizeLoc) });

        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlUpdate"]
            , data: param
            , success: function (msg) { }
            , error: function (req, txt, err) { alert("更新下载信息失败！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    //添加记录
    this.svr_create = function ()
    {
        this.ui.btn.down.hide();
        this.ui.msg.text("正在初始化...");
        var param = $.extend({}, this.fields,this.fileSvr, { time: new Date().getTime() });
        $.extend(param, { nameLoc: encodeURIComponent(this.fileSvr.nameLoc) });
        $.extend(param, { pathLoc: encodeURIComponent(this.fileSvr.pathLoc) });
        $.extend(param, { sizeSvr: encodeURIComponent(this.fileSvr.sizeSvr) });

        $.ajax({
            type: "get"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlFdCreate"]
            , data: param
            , success: function (msg)
            {
                _this.ui.btn.down.show();
                _this.ui.msg.text("初始化完毕...");
                _this.fileSvr.svrInit = true;
                _this.svr_create_cmp();
            }
            , error: function (req, txt, err) { alert("创建信息失败！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    this.svr_create_cmp = function () {
        setTimeout(function () {
            _this.down();
        }, 200);
    };

    this.isComplete = function () { return this.State == this.Config.state.Complete; };
    this.svr_delete = function ()
    {
        var param = $.extend({}, this.fields,{id:this.fileSvr.id,time:new Date().getTime()});
        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlDel"]
            , data: param
            , success: function (json){}
            , error: function (req, txt, err) { alert("删除数据错误！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    this.svr_delete_file = function (f_id)
    {
        var param = $.extend({}, this.fields, {id:f_id, time: new Date().getTime() });

        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: _this.Config["UrlDel"]
            , data: param
            , success: function (json) { }
            , error: function (req, txt, err) { alert("删除数据错误！" + req.responseText); }
            , complete: function (req, sta) { req = null; }
        });
    };

    this.down_complete = function (json)
    {
        this.Manager.filesCmp.push(this);
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.remove_wait(this.fileSvr.id);
        this.Manager.remove_url(this.fileSvr.nameLoc);
        this.hideBtns();
        this.event.downComplete(this);//biz event
        this.ui.btn.open.show();
        this.ui.process.css("width", "100%");
        this.ui.percent.text("(100%)");

        if (typeof (json.errFiles) != "undefined") {
            var fileErrs = new Array();
            $.each(json.errFiles, function (i, n) {
                var tmp = "<li class=\"txt-ico\"><img title=\"{error}\" src=\"/js/error.png\"/>名称：{name} 大小：{size} <a class=\"btn-link\" href=\"{url}\" target=\"_blank\">打开链接</a></li>";
                tmp = tmp.replace(/{name}/gi, n.name);
                tmp = tmp.replace(/{url}/gi, n.url);
                tmp = tmp.replace(/{size}/gi, n.sizeSvr);
                tmp = tmp.replace(/{error}/gi, _this.Config.errCode[n.errCode]);
                fileErrs.push(tmp);
            });
            this.ui.errFiles.html(fileErrs.join(""));
        }
        
        var msg = ["下载完毕", " 共:", json.fileCount,
            " 成功:", json.cmpCount,
            "(空:", json.emptyCount,
            ",重复:", json.repeatCount,
            ") 失败:", json.errCount,
            " 用时:", json.timeUse];

        this.ui.msg.text(msg.join());
        this.State = this.Config.state.Complete;
        this.svr_delete();
        setTimeout(function () { _this.Manager.down_next(); }, 500);
    };

    this.down_recv_size = function (json) {
        this.ui.size.text(json.sizeSvr);
        this.fileSvr.sizeSvr = json.size;
        this.fileSvr.lenSvr = json.len;
    };

    this.down_recv_name = function (json) {
        this.hideBtns();
        this.ui.btn.stop.show();
        this.ui.name.text(json.nameSvr);
        this.ui.name.attr("title", json.nameSvr);
        this.fileSvr.pathLoc = json.pathLoc;
    };

    this.down_process = function (json)
    {
        this.fileSvr.lenLoc = json.lenLoc;//保存进度
        this.fileSvr.sizeLoc = json.sizeLoc;
        this.fileSvr.perLoc = json.percent;
        //更新文件进度
        //this.fileSvr.files[json.file.id];
        //this.fileSvr.files[json.file.id].lenLoc = json.file.lenLoc;
        //this.fileSvr.files[json.file.id].percent = json.file.percent;

        this.ui.percent.text("("+json.percent+")");
        this.ui.process.css("width", json.percent);
        var msg = [json.fileIndex + 1, "/", json.filesCount, " ", json.sizeLoc, " ", json.speed, " ", json.time];
        this.ui.msg.text(msg.join(""));
    };

    //更新服务器进度
    this.down_part = function (json)
    {
    };

    this.init_end = function (json)
    {
        $.extend(true,this.fileSvr, json);
        this.svr_create();//添加记录
    };

    this.down_begin = function (json)
    {
    };

    this.down_error = function (json)
    {
        this.hideBtns();
        this.ui.btn.down.show();
        this.ui.btn.del.show();
        this.event.downError(this, json.code);//biz event
        this.ui.msg.text(this.Config.errCode[json.code + ""]);
        this.State = this.Config.state.Stop;
        this.svr_update();
        
        var errors = new Array();
        var tmp = "<li class=\"m-t-xs\">文件：{count},\
        完成:{cmp}(空:{empty},重复:{repeat}),\
        错误:{err}(网络:{network},长路径:{lpath},空间不足:{full})\
        ,用时:{timeUse}</li>";
        tmp = tmp.replace(/{count}/gi, json.fileCount);
        tmp = tmp.replace(/{empty}/gi, json.emptyCount);
        tmp = tmp.replace(/{cmp}/gi, json.cmpCount);
        tmp = tmp.replace(/{err}/gi, json.errCount);
        tmp = tmp.replace(/{network}/gi, json.netCount);
        tmp = tmp.replace(/{repeat}/gi, json.repeatCount);
        tmp = tmp.replace(/{lpath}/gi, json.lpathCount);
        tmp = tmp.replace(/{full}/gi, json.fullCount);
        tmp = tmp.replace(/{timeUse}/gi, json.timeUse);
        errors.push(tmp);

        if (typeof (json.errFiles) != "undefined") {
            $.each(json.errFiles, function (i, n) {
                tmp = "\
                <li>\
                    <div class=\"txt-ico m-t-xs txt-limit-sm\">\
                        <a title=\"{name}\" class=\"txt-link\" href=\"{url}\" target=\"_blank\"><img name=\"err\"/>{name}</a>\
                    </div>\
                    <div style=\"margin:3px 0 0 0;\">大小：{size}</div>\
                    <div class=\"txt-limit-sm m-t-xs\" title=\"{path}\">路径：{path}</div>\
                    <div class=\"m-t-xs\">错误：{error}</div>\
                    <div class=\"m-t-xs\">错误码：{errCode}</div>\
                </li>";
                tmp = tmp.replace(/{name}/gi, n.name);
                tmp = tmp.replace(/{url}/gi, n.url);
                tmp = tmp.replace(/{size}/gi, n.sizeSvr);
                tmp = tmp.replace(/{error}/gi, _this.Config.errCode[n.errCode]);
                tmp = tmp.replace(/{path}/gi, n.pathLoc);
                tmp = tmp.replace(/{errCode}/gi, n.errCodeNet);
                errors.push(tmp);
            });
        }
        this.ui.errFiles.html(errors.join(""));
        this.ui.errFiles.find("img[name='err']").each(function(n,i){
            $(this).attr("src",_this.Manager.data.ico.err);
        });

        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.add_wait(this.fileSvr.id);
    };

    this.down_stoped = function (json)
    {
        this.hideBtns();
        this.ui.btn.down.show();
        this.ui.btn.del.show();
        this.svr_update();
        this.Manager.del_work(this.fileSvr.id);//从工作队列中删除
        this.Manager.add_wait(this.fileSvr.id);
        this.ui.msg.text("下载已停止");
    };
}