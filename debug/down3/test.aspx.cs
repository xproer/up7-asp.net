﻿using System;
using System.Text;
using up7.db.utils;

namespace up7.down3.debug
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string v = "F%3A%2Fcsharp%2Fapps%2Fup7%2Fupload%2F2019%2F05%2F08%2F33943567d45f40e7b55c5d03fcd378eb%2Ftest%2FVMware%5FWorkstation%5F7%2E1%2E4%2D%E5%A9%B5%EE%86%BE%E6%87%93%E9%A1%A6%EE%88%9C%E2%82%AC%E5%9E%AB%E7%85%A1%E5%AE%95%3Frar";

            var pathSvr = PathTool.url_decode(v);
            Response.Write(pathSvr);
            Response.Write("<br/>");

            var utf8 = UTF8Encoding.UTF8;
            var pathSvrBytes = utf8.GetBytes(pathSvr);
            Encoding gb2312Encoding = Encoding.GetEncoding("gb2312"); // GB2132 encoding

            byte[] bytes = Encoding.Convert(utf8, gb2312Encoding, pathSvrBytes);
            pathSvr = gb2312Encoding.GetString(bytes);

            Response.Write(pathSvr);
        }
    }
}