﻿using Newtonsoft.Json.Linq;
using System;
using up7.db.model;
using up7.db.sql;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class uncmp : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int uid = this.reqInt("uid");
            var files = SqlTable.build("up7_files").reads<FileInf>(
                "f_id ,f_nameLoc ,f_pathLoc ,f_sizeLoc ,f_lenSvr ,f_perSvr ,f_fdTask ,f_md5 ",
                SqlWhere.build()
                .eq("f_complete", false)
                .eq("f_deleted", false)
                .eq("f_uid", uid),
                SqlSort.build());

            this.toContentJson(JToken.FromObject(files));
        }
    }
}