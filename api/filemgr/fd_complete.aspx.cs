﻿using System;
using up7.db.biz;
using up7.db.database.up7.sql;
using up7.db.filemgr;
using up7.db.model;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqStr("id");
            string pathRel = this.reqStrDecode("pathRel");
            var uid = this.reqInt("uid");
            string cak = this.reqStr("callback");
            int cover = this.reqInt("cover");
            int ret = 0;

            if (string.IsNullOrEmpty(id))
            {
            }
            else
            {
                FileInf folder = null;
                FileInf fdExist = null;

                //当前目录是根目录?
                bool isRootDir = pathRel == "/";
                //根目录
                if (isRootDir)
                {
                    folder = SqlFile.build().read(id);
                    //查询同级同名目录
                    fdExist = SqlFile.build().read(folder.pathRel, id, folder.uid);

                    //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
                    if (fdExist != null) SqlFile.build().del(uid, fdExist.id);
                }
                //子目录
                else
                {
                    folder = SqlFolder.build().read(id);
                    folder.uid = uid;
                    fdExist = SqlFolder.build().read(folder.pathRel, id, folder.uid);

                    //存在相同目录=>直接使用已存在的目录信息=>删除旧目录
                    if (fdExist != null) SqlFolder.build().del(fdExist.id, uid);
                }

                //存在同名，同路径文件
                if (fdExist != null)
                {
                    folder.id = fdExist.id;
                    folder.pid = fdExist.pid;
                    folder.pidRoot = fdExist.pidRoot;
                }

                //根节点
                FileInf root = new FileInf();
                root.id = folder.pidRoot;
                root.uid = folder.uid;
                //当前节点是根节点
                if (string.IsNullOrEmpty(root.id)) root.id = folder.id;

                //保存层级结构-解析层级信息文件
                FolderScaner fsd = new FolderScaner();
                fsd.m_cover = fdExist != null;
                fsd.save(folder);

                //上传完毕=>子目录
                if (!isRootDir) SqlFolder.build().complete(id, uid);
                //根目录
                else SqlFile.build().complete(id);

                up7_biz_event.folder_post_complete(id);

                ret = 1;
            }
            this.toContentJson(cak + "(" + ret + ")");
        }
    }
}