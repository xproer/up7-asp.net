﻿using Newtonsoft.Json.Linq;
using System;
using up7.db.database;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = this.reqStr("id");
            var pathRel = this.reqStrDecode("pathRel");
            pathRel += '/';

            SqlWhereMerge swm = new SqlWhereMerge();
            DBConfig cfg = new DBConfig();
            var tp = ConfigReader.dbType();
            if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC || tp == DataBaseType.Oracle)
            {
                swm.instr(pathRel, "f_pathRel");
            }
            else
            {
                swm.charindex(pathRel, "f_pathRel");
            }
            string where = swm.to_sql();

            SqlExec se = cfg.se();
            se.update("up7_folders"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , where
                );

            se.update("up7_folders"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , new SqlParam[] { new SqlParam("f_id", id) }
                );

            se.update("up7_files"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , where
                );

            se.update("up7_files"
                , new SqlParam[] { new SqlParam("f_deleted", true) }
                , new SqlParam[] { new SqlParam("f_id", id) }
                );

            this.toContent(new JObject { { "ret", 1 } });
        }
    }
}