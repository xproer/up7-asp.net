﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using up7.db.database;
using up7.db.model;
using up7.db.sql;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class rename : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var id = this.reqStr("id");
            var folder = this.reqBool("fdTask");
            var pathRel = this.reqStrDecode("pathRel");
            var nameNew = this.reqStrDecode("nameLoc");           

            //当前目录是根目录?
            bool isRootDir = PathTool.parentDir(pathRel) == "/";

            if (!folder)
            {
                this.renameFile(id,pathRel,nameNew);
            }
            //是子目录
            else if (!isRootDir)
            {
                this.renameChildDir(id,pathRel,nameNew);
            }
            //是根目录
            else
            {
                this.renameRootDir(id,pathRel,nameNew);
            }
        }

        void renameFile(string id, string pathRel, string name)
        {
            //新的相对路径
            pathRel = Path.Combine(PathTool.parentDir(pathRel), name);
            pathRel = pathRel.Replace('\\', '/');

            //根据相对路径(新)=>查找同名文件
            var s = new FileInf();
            s = SqlTable.build("up7_files").readOne<FileInf>(
                //相对路径=>/dir
                SqlWhere.build()
                .eq("f_pathRel", pathRel)
                .eq("f_deleted", false)
                );

            bool exist = s != null;

            //不存在同名文件
            if (!exist)
            {
                SqlTable.build("up7_files").update(
                    SqlSeter.build()
                    .set("f_nameLoc", name)
                    .set("f_nameSvr", name)
                    .set("f_pathRel", pathRel),
                    SqlWhere.build().eq("f_id", id)
                    );

                var ret = new JObject { { "state", true },
                    { "pathRel",pathRel} };
                this.toContent(ret);
            }
            //存在同名项
            else
            {
                var res = new JObject { { "state", false }, { "msg", "存在同名文件" }, { "code", "102" } };
                this.toContent(res);
            }
        }
        void renameChildDir(string id, string pathRel, string name)
        {
            var pathRelOld = pathRel;
            //root/dir/name => root/dir
            var index = pathRel.LastIndexOf("/");
            pathRel = pathRel.Substring(0, index + 1);
            //root/dir/old => root/dir/new
            pathRel += name;
            var pathRelNew = pathRel;

            var fd = SqlTable.build("up7_folders").readOne<FileInf>("f_id,f_pathRel",
                SqlWhere.build()
                .eq("f_pathRel", pathRelNew)
                .eq("f_deleted", false)
                );
            bool exist = fd != null;

            //不存在同名目录
            if (!exist)
            {
                //更新相对路径
                SqlTable.build("up7_folders").update(
                    SqlSeter.build()
                    .set("f_nameLoc", name)
                    .set("f_pathRel", pathRelNew),
                    SqlWhere.build().eq("f_id", id)
                    );

                //更新子级文件和目录路径
                this.folder_renamed(pathRelOld, pathRelNew);
                var v = new JObject {
                    { "state", true },
                    { "pathRel",pathRelNew}
                };
                this.toContent(v);
            }
            //存在同名项
            else
            {
                var v = new JObject { { "state", false }, { "msg", "存在同名目录" }, { "code", "102" } };
                this.toContent(v);
            }
        }
        void renameRootDir(string id, string pathRel, string name)
        {
            var pathRelOld = pathRel;
            //新的相对路径
            var pathRelNew = "/" + name;

            var s = SqlTable.build("up7_files").readOne<FileInf>(
                //相对路径=>/dir
                SqlWhere.build()
                .eq("f_pathRel", pathRelNew)
                .eq("f_deleted", false)
                );

            bool exist = s != null;

            //不存在同名目录
            if (!exist)
            {
                SqlTable.build("up7_files").update(
                    SqlSeter.build()
                    .set("f_nameLoc", name)
                    .set("f_nameSvr", name)
                    .set("f_pathRel", pathRelNew),
                    SqlWhere.build().eq("f_id", id)
                    );

                //更新子级文件和目录路径
                this.folder_renamed(pathRelOld, pathRelNew);

                var v = new JObject {
                    { "state", true },
                    { "pathRel",pathRelNew}
                };
                this.toContent(v);
            }
            //存在同名项
            else
            {
                var v = new JObject { { "state", false }, { "msg", "存在同名目录" }, { "code", "102" } };
                this.toContent(v);
            }
        }
        void folder_renamed(string pathRelOld, string pathRelNew)
        {
            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();
            string sql = string.Format("update up7_files set f_pathRel=REPLACE(f_pathRel,'{0}/','{1}/') where CHARINDEX('{0}/',f_pathRel)=1",
                pathRelOld,
                pathRelNew
                );
            if (cfg.m_isOracle)
            {
                sql = string.Format("update up7_files set f_pathRel=REPLACE(f_pathRel,'{0}/','{1}/') where instr(f_pathRel,'{0}/')=1",
                pathRelOld,
                pathRelNew
                );
            }
            se.exec(sql);

            //更新目录表
            sql = string.Format("update up7_folders set f_pathRel=REPLACE(f_pathRel,'{0}/','{1}/') where CHARINDEX('{0}/',f_pathRel)=1",
                pathRelOld,
                pathRelNew
                );
            if (cfg.m_isOracle)
            {
                sql = string.Format("update up7_folders set f_pathRel=REPLACE(f_pathRel,'{0}/','{1}/') where instr(f_pathRel,'{0}/')=1",
                pathRelOld,
                pathRelNew
                );
            }
            se.exec(sql);
        }
    }
}