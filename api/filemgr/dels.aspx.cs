﻿using Newtonsoft.Json.Linq;
using System;
using up7.db.database;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class f_dels : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var par = Request.Form["data"];
            par = Server.UrlDecode(par);
            var obj = JToken.Parse(par);

            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();

            //更新文件
            se.exec_batch("up7_files"
                , "update up7_files set f_deleted=1 where f_id=@f_id"
                , string.Empty
                , "f_id"
                , obj);

            //更新文件夹
            se.exec_batch("up7_folders"
                , "update up7_folders set f_deleted=1 where f_id=@f_id"
                , string.Empty
                , "f_id"
                , obj);

            this.toContent(obj);
        }
    }
}