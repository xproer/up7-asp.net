﻿using Newtonsoft.Json;
using System;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class fd_data : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqStr("id");
            string cbk = this.reqStr("callback");
            string json = "({\"value\":null})";

            if (!string.IsNullOrEmpty(id))
            {
                FolderBuilder fb = new FolderBuilder();
                var data = JsonConvert.SerializeObject(fb.build(id));
                data = this.Server.UrlEncode(data);
                data = data.Replace("+", "%20");

                json = "({\"value\":\"" + data + "\"})";
            }
            this.toContent(cbk + json);
        }
    }
}