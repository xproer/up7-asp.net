﻿using System;
using up7.db.biz;
using up7.db.database.up7.sql;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    /// <summary>
    /// 文件上传完毕
    ///  处理是否合并的逻辑
    /// </summary>
    public partial class f_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var uid = this.reqInt("uid");
            var id = this.reqStr("id");
            var cbk = this.reqStr("callback");
            var cover = this.reqInt("cover");//是否覆盖

            //返回值。1表示成功
            int ret = 0;

            if (string.IsNullOrEmpty(id))
            {
            }//参数不为空
            else
            {
                var db = SqlFile.build();
                db.complete(id);
                var file = db.read(id);

                //覆盖同名文件-更新同名文件状态
                if (cover == 1) db.delete(file.pathRel, uid, id);                

                //触发事件
                up7_biz_event.file_post_complete(id);
                ret = 1;
            }
            
            Response.Write(cbk + "(" + ret + ")");//必须返回jsonp格式数据
        }
    }
}