﻿using Newtonsoft.Json.Linq;
using System;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class path : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            var data = this.reqStr("data");
            data = Server.UrlDecode(data);
            var fd = JObject.Parse(data);

            DbFolder df = new DbFolder();
            var d = df.build_path(fd);

            this.toContentJson(JToken.FromObject(d));
        }
    }
}