﻿using Newtonsoft.Json.Linq;
using System;
using up7.db.model;
using up7.db.sql;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class mkdir : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var nameLoc = this.reqStrDecode("nameLoc");
            var pid = this.reqStr("pid");
            var uid = this.reqInt("uid");
            var pidRoot = this.reqStr("pidRoot");
            var pathRel = this.reqStrDecode("pathRel");
            bool isRootDir = pathRel == "/";
            pathRel = PathTool.combin(pathRel, nameLoc);

            DbFolder df = new DbFolder();
            if (df.exist_same_folder(pathRel))
            {
                var ret = new JObject { { "ret", false }, { "msg", "已存在同名目录" } };
                this.toContent(ret);
                return;
            }

            FileInf dir = new FileInf();
            dir.id = Guid.NewGuid().ToString("N");
            dir.pid = pid;
            dir.uid = uid;
            dir.pidRoot = pidRoot;
            dir.nameLoc = nameLoc;
            dir.nameSvr = nameLoc;
            dir.complete = true;
            dir.fdTask = true;
            dir.pathRel = pathRel;

            //根目录
            if (isRootDir)
            {
                SqlTable.build("up7_files").insert(dir);
            }//子目录
            else
            {
                SqlTable.build("up7_folders").insert(dir);
            }

            JObject obj = JObject.FromObject(dir);
            obj["ret"] = true;
            this.toContent(obj);
        }
    }
}