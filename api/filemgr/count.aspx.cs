﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up7.down3.biz;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class count : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqStr("id");
            string page = this.reqStr("page");

            if (string.IsNullOrEmpty(id))
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "id param is null";
                return;
            }

            FolderBuilder fb = new FolderBuilder();
            var c = fb.count(id);
            var totalSz = fb.totalSize(id);
            var obj = new JObject { { "count", c }, { "totalSize", totalSz } };
            this.toContent(obj);
        }
    }
}