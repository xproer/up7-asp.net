﻿using Newtonsoft.Json.Linq;
using System;
using up7.db.sql;
using up7.down3.model;
using up7.filemgr.app;

namespace up7.api.filemgr
{
    public partial class uncmp_down : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int uid = this.reqInt("uid");
            var fs = SqlTable.build("down3_files").reads<DnFileInf>(
                "f_id,f_nameLoc,f_pathLoc,f_perLoc,f_sizeSvr,f_fdTask",
                SqlWhere.build().eq("f_uid", uid),
                SqlSort.build());

            this.toContentJson(JToken.FromObject(fs));
        }
    }
}