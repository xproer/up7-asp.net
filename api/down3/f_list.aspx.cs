﻿using System;
using System.Web;
using up7.db.database;
using up7.down3.biz;
using up7.filemgr.app;

namespace up7.down3.db
{
    /// <summary>
    /// 列出未下载完的任务
    /// </summary>
    public partial class f_list : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var uid = this.reqInt("uid");
            var cbk = this.reqStr("callback");

            string json = DnFile.build().all_uncmp(uid);
            if (!string.IsNullOrEmpty(json))
            {
                json = HttpUtility.UrlEncode(json);
                json = json.Replace("+", "%20");
                json = cbk + "({\"value\":\"" + json + "\"})";
            }
            else { json = cbk + "({\"value\":null})"; };

            this.toContentJson(json);
        }
    }
}