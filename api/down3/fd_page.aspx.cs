﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Web;
using up7.db.database;
using up7.db.database.up7.sql;
using up7.filemgr.app;

namespace up7.down3.db
{
    /// <summary>
    /// 获取文件夹数据，以分页方式获取，
    /// </summary>
    public partial class fd_page : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string op = this.reqStr("op");//
            string id = this.reqStr("id");//

            if (op == "count")
            {
                var count = SqlFolder.count(id);
                var obj = new JObject { { "count", count } };
                this.toContent(obj);
            }
            else {
                string index = Request.QueryString["page"];//页数，基于1

                if (string.IsNullOrEmpty(id)
                    || string.IsNullOrEmpty(index))
                {
                    Response.Write("");
                    return;
                }

                DBConfig cfg = new DBConfig();
                var p = cfg.fp();
                string json = p.read(index, id);
                json = HttpUtility.UrlEncode(json);
                this.toContent(json);
            }
        }
    }
}