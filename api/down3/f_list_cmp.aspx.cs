﻿using System;
using System.Web;
using up7.db.database;
using up7.down3.biz;
using up7.filemgr.app;

namespace up7.down3.db
{
    /// <summary>
    /// 从up7_files表中加载所有已经上传完毕的文件和文件夹
    /// </summary>
    public partial class f_list_cmp : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var uid = this.reqInt("uid");
            var cbk = this.reqStr("callback");//jsonp

            string json = DnFile.build().all_complete(uid);
            json = HttpUtility.UrlEncode(json);
            //UrlEncode会将空格解析成+号
            json = json.Replace("+", "%20");
            this.toContentJson(cbk + "({\"value\":\"" + json + "\"})");
        }
    }
}