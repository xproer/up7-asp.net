﻿using System;
using up7.db.biz;
using up7.db.database;
using up7.db.database.up7.sql;
using up7.db.model;
using up7.filemgr.app;

namespace up7.db
{
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id    = this.reqStr("id");
            string uid   = this.reqStr("uid");
            string cak   = this.reqStr("callback");
            int ret = 0;

            //参数为空
            if (!string.IsNullOrEmpty(id) )
            {
                var db = SqlFile.build();
                FileInf folder = db.read(id);

                //根节点
                FileInf root = new FileInf();
                root.id = folder.pidRoot;
                root.uid = folder.uid;
                //当前节点是根节点
                if (string.IsNullOrEmpty(root.id)) root.id = folder.id;

                //保存层级结构-解析层级信息文件
                FolderSchemaDB fsd = new FolderSchemaDB();
                fsd.save(folder);

                //上传完毕
                db.complete(id);

                ret = 1;

                //触发事件
                up7_biz_event.folder_post_complete(id);
            }
            this.toContentJson(cak + "(" + ret + ")");
        }
    }
}