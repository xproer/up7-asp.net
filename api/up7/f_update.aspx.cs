﻿using System;
using up7.db.database.up7.sql;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.db
{
    /// <summary>
    /// 更新文件或文件夹进度信息
    /// 在停止时调用
    /// 在出错时调用
    /// </summary>
    public partial class f_update : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqStr("id");
            int uid = this.reqInt("uid");
            long offset = this.reqLong("offset");
            long lenSvr = this.reqLong("lenSvr");
            string perSvr = this.reqStr("perSvr");
            string callback = this.reqStr("callback");//jsonp参数

            //参数为空
            if (    string.IsNullOrEmpty(id))
            {
                XDebug.Output("uid", uid);
                XDebug.Output("idSvr", id);
                Response.Write("param is null");
                return;
            }

            SqlFile.build().process(uid, id, offset, lenSvr, perSvr);
        }
    }
}