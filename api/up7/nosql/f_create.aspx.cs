﻿using Newtonsoft.Json;
using System;
using System.Web;
using up7.db.biz;
using up7.db.model;
using up7.db.utils;
using up7.filemgr.app;
using System.IO;

namespace up7.api.up7.nosql
{
    public partial class f_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string callback = string.Empty;
            FileInf fileSvr = new FileInf();
            this.request("id", out fileSvr.id)
                .request("uid", out fileSvr.uid)
                .request("sizeLoc", out fileSvr.sizeLoc)
                .request("lenLoc", out fileSvr.lenLoc)
                .request("blockSize", out fileSvr.blockSize)
                .request("blockCount", out fileSvr.blockCount)
                .request("pathLoc", out fileSvr.pathLoc)
                .request("callback", out callback);
            fileSvr.pathLoc = PathTool.url_decode(fileSvr.pathLoc);
            fileSvr.nameLoc = Path.GetFileName(fileSvr.pathLoc);
            fileSvr.nameSvr = fileSvr.nameLoc;

            //参数为空
            if (
                string.IsNullOrEmpty(fileSvr.sizeLoc) ||
                string.IsNullOrEmpty(fileSvr.pathLoc)
                )
            {
                var j = JsonConvert.SerializeObject(fileSvr);
                this.toContent(callback + "(" + j + ")", 500);
                return;
            }

            PathGuidBuilder pb = new PathGuidBuilder();
            fileSvr.pathSvr = pb.genFile(fileSvr.uid, fileSvr.id, fileSvr.nameLoc);

            //触发事件
            up7_biz_event.file_create(fileSvr);

            string jv = JsonConvert.SerializeObject(fileSvr);
            jv = HttpUtility.UrlEncode(jv);
            jv = jv.Replace("+", "%20");
            var json = callback + "({\"value\":\"" + jv + "\"})";//返回jsonp格式数据。
            this.toContent(json);
        }
    }
}