﻿using System;
using up7.filemgr.app;

namespace up7.api.up7.nosql
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string uid = Request.QueryString["uid"];
            string cbk = Request.QueryString["callback"];
            int ret = 0;

            if (string.IsNullOrEmpty(id)||
                string.IsNullOrEmpty(uid))
            {
            }//参数不为空
            else
            {
                ret = 1;
            }
            Response.Write(cbk + "(" + ret + ")");//返回jsonp格式数据
        }
    }
}