﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up7.db.database;
using up7.db.biz;
using up7.db.model;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.up7.nosql
{
    public partial class fd_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id, pathLoc, sizeLoc, callback;

            long lenLoc = 0;
            int fCount, uid;

            this.request("id", out id)
                .request("pathLoc", out pathLoc)
                .request("sizeLoc", out sizeLoc)
                .request("lenLoc", out lenLoc)
                .request("uid", out uid)
                .request("filesCount", out fCount)
                .request("callback", out callback);
            pathLoc = PathTool.url_decode(pathLoc);

            FileInf f = new FileInf();
            f.nameLoc = Path.GetFileName(pathLoc);
            f.nameSvr = f.nameLoc;
            f.id = id;
            f.pathLoc = pathLoc;
            f.sizeLoc = sizeLoc;
            f.lenLoc = lenLoc;
            f.fileCount = fCount;
            f.fdTask = true;
            f.uid = uid;
            //生成路径，格式：upload/年/月/日/guid/文件夹名称
            PathGuidBuilder pb = new PathGuidBuilder();
            f.pathSvr = Path.Combine(pb.genFolder(f.uid, f.id), f.nameLoc);
            f.pathSvr = f.pathSvr.Replace("\\", "/");
            var dir = Directory.CreateDirectory(f.pathSvr);
            //目录创建失败
            if (!dir.Exists)
            {
                this.toContent("create dir error", 500);
                return;
            }

            //触发事件
            up7_biz_event.folder_create(f);

            string json = JsonConvert.SerializeObject(f);
            json = HttpUtility.UrlEncode(json);
            json = json.Replace("+", "%20");
            Response.Write(callback + "({\"value\":\"" + json + "\"})");
        }
    }
}