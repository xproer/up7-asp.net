﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up7.db.database;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.up7.nosql
{
    public partial class f_update : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = Request.QueryString["uid"];
            string id = Request.QueryString["id"];
            string perSvr = Request.QueryString["perSvr"];//文件百分比
            string blockSize = Request.QueryString["blockSize"];//本地文件大小

            //参数为空
            if (string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(id)
                )
            {
                XDebug.Output("uid", uid);
                XDebug.Output("idSvr", id);
                Response.Write("param is null");
                return;
            }
        }
    }
}