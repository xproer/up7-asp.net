﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up7.db.database;
using up7.db.biz;
using up7.db.model;
using up7.filemgr.app;

namespace up7.api.up7.nosql
{
    public partial class fd_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            string uid = Request.QueryString["uid"];
            string cak = Request.QueryString["callback"];
            int ret = 0;

            //参数为空
            if (!string.IsNullOrEmpty(id))
            {
                ret = 1;

                //触发事件
                up7_biz_event.folder_post_complete(id);
            }
            Response.Write(cak + "(" + ret + ")");
        }
    }
}