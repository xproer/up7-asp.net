﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using up7.db.database;
using up7.db.biz;
using up7.filemgr.app;

namespace up7.api.up7.nosql
{
    public partial class f_complete : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string uid = Request.QueryString["uid"];
            string id = Request.QueryString["id"];
            string cbk = Request.QueryString["callback"];

            //返回值。1表示成功
            int ret = 0;

            if (string.IsNullOrEmpty(uid)
                || string.IsNullOrEmpty(id))
            {
            }//参数不为空
            else
            {
                ret = 1;

                //触发事件
                up7_biz_event.file_post_complete(id);
            }

            Response.Write(cbk + "(" + ret + ")");//必须返回jsonp格式数据
        }
    }
}