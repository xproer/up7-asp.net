﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text;
using up7.db.biz;
using up7.db.model;
using up7.db.utils;
using up7.filemgr.app;

namespace up7.api.up7
{
    /// <summary>
    /// 接收控件传递的子文件信息
    /// </summary>
    public partial class f_notice : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(Request.Form.ToString());
            //无文件数据
            if (Request.Files.Count < 1)
            {
                var pars = new JObject {
                    { "detail","file data empty"}
                };
                this.toContent(pars, 500);
                return;
            }
            var file = Request.Files[0];
            var txt =  Encoding.UTF8.GetString(UtilsTool.toBytes(file.InputStream));
            txt = Server.UrlDecode(txt);
            var fsv = JsonConvert.DeserializeObject<FileInf>(txt);

            fsv.saveScheme();
            this.toContentJson(new JObject { { "state", true } });
        }
    }
}