﻿using System;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using up7.db.biz;
using up7.db.model;
using up7.db.utils;
using up7.db.database;
using up7.filemgr.app;
using Newtonsoft.Json.Linq;
using up7.db.database.up7.sql;
using System.Security.Cryptography;

namespace up7.db
{
    /// <summary>
    /// 此文件处理单文件上传逻辑
    /// 此页面需要返回文件的pathSvr路径。并进行urlEncode编码
    /// 更新记录：
    ///     2016-03-23 优化逻辑，分享子文件逻辑
    /// </summary>
    public partial class f_create : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FileInf fileSvr      = new FileInf();
            fileSvr.id = this.reqStr("id");
            fileSvr.uid = this.reqInt("uid");
            fileSvr.md5 = this.reqStr("md5");
            fileSvr.sizeLoc = this.reqStr("sizeLoc");
            fileSvr.lenLoc = this.reqLong("lenLoc");
            fileSvr.lenLocSec = this.reqLong("lenLocSec");
            fileSvr.blockSize = this.reqInt("blockSize");
            fileSvr.blockCount = this.reqInt("blockCount");
            fileSvr.pathLoc = this.reqStrDecode("pathLoc");
            fileSvr.storage = this.reqStr("storage");
            fileSvr.objectKey = this.reqStr("objectKey");
            fileSvr.objectUrl = this.reqStr("objectUrl");
            fileSvr.nameLoc = Path.GetFileName(fileSvr.pathLoc);
            fileSvr.nameSvr = fileSvr.nameLoc;
            fileSvr.encryptAlgorithm = this.reqStr("encrypt-algorithm");
            string callback = this.reqStr("callback");

            //参数为空
            if (
                string.IsNullOrEmpty(fileSvr.sizeLoc)||
                string.IsNullOrEmpty(fileSvr.pathLoc) 
                )
            {
                var j = JsonConvert.SerializeObject(fileSvr);
                this.toContent(callback + "("+j+")", 500);
                return;
            }
            
            PathGuidBuilder pb   = new PathGuidBuilder();
            fileSvr.pathSvr      = pb.genFile(fileSvr.uid, fileSvr.id,fileSvr.nameLoc);

            var dbFile = SqlFile.build();
            FileInf fileExist = dbFile.exist_file(fileSvr.md5);
            //数据库存在相同文件
            if (null != fileExist)
            {
                fileSvr.nameSvr = fileExist.nameSvr;
                fileSvr.pathSvr = fileExist.pathSvr;
                fileSvr.pathRel = fileExist.pathRel;
                fileSvr.perSvr = fileExist.perSvr;
                fileSvr.lenSvr = fileExist.lenSvr;
                fileSvr.complete = fileExist.complete;
                fileSvr.encrypt = fileExist.encrypt;
                fileSvr.encryptAlgorithm = fileExist.encryptAlgorithm;
                fileSvr.lenLocSec = fileExist.lenLocSec;
                fileSvr.blockSize = fileExist.blockSize;
                fileSvr.objectKey = fileExist.objectKey;
                fileSvr.objectUrl = fileExist.objectUrl;
                fileSvr.storage = fileExist.storage;
                dbFile.Add(fileSvr);

                //触发事件
                up7_biz_event.file_create_same(fileSvr);
            }//数据库不存在相同文件
            else
            {
                try
                {
                    //创建文件
                    var fw = ConfigReader.blockWriter();
                    if(fileSvr.storage=="io")fw.make(fileSvr);
                }
                catch (IOException ie)
                {
                    var obj = new JObject { { "value", "" }, { "error", ie.Message }, { "ret", false } };
                    this.toContentJson(callback + "(" + obj.ToString() + ")");
                    return;
                }

                dbFile.Add(fileSvr);
                //触发事件
                up7_biz_event.file_create(fileSvr);
            }

            //将路径转换成相对路径
            fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);

            string jv = JsonConvert.SerializeObject(fileSvr);
            jv = HttpUtility.UrlEncode(jv);
            jv = jv.Replace("+", "%20");
            var json = callback + "({\"value\":\"" + jv + "\",\"ret\":true})";//返回jsonp格式数据。
            this.toContentJson(json);
        }
    }
}