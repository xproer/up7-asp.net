﻿using System;
using up7.db.database.up7.sql;
using up7.filemgr.app;

namespace up7.db
{
    public partial class f_del : WebBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = this.reqStr("id");
            int uid = this.reqInt("uid");
            string cbk = this.reqStr("callback");
            int ret = 0;

            if (string.IsNullOrEmpty(id))
            {
            }//参数不为空
            else
            {
                SqlFile.build().del(uid,id);
                ret = 1;
            }
            Response.Write(cbk + "(" + ret + ")");//返回jsonp格式数据
        }
    }
}