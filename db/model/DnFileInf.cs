﻿using up7.db.model;
using up7.db.sql.model;

namespace up7.down3.model
{
    public class DnFileInf : FileInf
    {
        public DnFileInf()
        {
        }

        [DataBase("f_mac")]
        public string mac = string.Empty;

        public string f_id = string.Empty;//与up7_files.f_id表关联
        
        [DataBase("f_fileUrl")]
        public string fileUrl = string.Empty;
        
        //本地已下载百分比
        [DataBase("f_perLoc")]
        public string perLoc = "0%";
    }
}