﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace up7.db.utils
{
    public enum StorageType
    {
        IO, FastDFS, Minio, OSS, OBS
    }
    public enum DataBaseType
    {
        SqlServer, Oracle, MySQL, MongoDB, ODBC, Kingbase
    }

    /// <summary>
    /// 系统配置文件读取
    /// ConfigReader cr = new ConfigReader();
    /// cr.module("名称");//获取json对象
    /// </summary>
    public class ConfigReader
    {
        public JToken m_files;
        public string io_dir = string.Empty;

        /// <summary>
        /// 自动加载/data/config/config.json配置文件
        /// </summary>
        public ConfigReader()
        {
            string file = HttpContext.Current.Server.MapPath("/config/config.json");
            this.m_files = JToken.Parse(File.ReadAllText(file));
            this.io_dir = this.m_files.SelectToken("$.io.dir").ToString().Trim();
        }
        public static DataBaseType dbType()
        {
            var cr = new ConfigReader();
            var tp = cr.readString("database.connection.type");
            if (string.Compare(tp, "oracle", true) == 0) return DataBaseType.Oracle;
            else if (string.Compare(tp, "kingbase", true) == 0) return DataBaseType.Kingbase;
            else if (string.Compare(tp, "mongodb", true) == 0) return DataBaseType.MongoDB;
            return DataBaseType.SqlServer;
        }



        /// <summary>
        /// 存储类型:磁盘存储(IO),分布式存储(FastDFS)
        /// </summary>
        /// <returns></returns>
        public static StorageType storage()
        {
            var cr = new ConfigReader();
            var sto = cr.m_files.SelectToken("$.Storage.type").ToString().Trim().ToLower();
            if (string.Compare(sto, "fastdfs", true) == 0) return StorageType.FastDFS;
            else if (string.Compare(sto, "minio", true) == 0) return StorageType.Minio;
            else if (string.Compare(sto, "oss", true) == 0) return StorageType.OSS;
            else if (string.Compare(sto, "obs", true) == 0) return StorageType.OBS;
            return StorageType.IO;
        }

        /// <summary>
        /// 传输加密
        /// 配置：/config.json/security.encrypt
        /// </summary>
        /// <returns></returns>
        public static bool postEncrypt()
        {
            ConfigReader cr = new ConfigReader();
            var sec = cr.m_files;
            var encrypt = (bool)cr.m_files.SelectToken("$.security.encrypt");
            return encrypt;
        }

        public static bool securityToken()
        {
            ConfigReader cr = new ConfigReader();
            var sec = cr.m_files;
            var encrypt = (bool)cr.m_files.SelectToken("$.security.token");
            return encrypt;
        }

        /// <summary>
        /// 存储加密，需要在前端开启传输加密
        /// /up6.js/security.encrypt
        /// </summary>
        /// <returns></returns>
        public static bool storageEncrypt()
        {
            var cr = new ConfigReader();
            var e = bool.Parse(cr.m_files.SelectToken("$.Storage.encrypt").ToString().Trim());
            return e;
        }

        public static FileBlockWriter blockWriter()
        {
            //var sto = storage();
            //if (sto == StorageType.FastDFS) return new FastDFSWriter();
            //else if (sto == StorageType.Minio) return new MinioWriter();
            //else if (sto == StorageType.OSS) return new OSSWriter();
            //else if (sto == StorageType.OBS) return new OBSWriter();
            //else return new FileBlockWriter();
            return new FileBlockWriter();
        }

        /// <summary>
        /// 自动加载模块
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public JToken module(string name)
        {
            string file = (string)this.m_files.SelectToken(name);
            file = HttpContext.Current.Server.MapPath(file);
            var o = JToken.Parse( File.ReadAllText(file ));
            return o;
        }

        /// <summary>
        /// 读取config.json中的值
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string readString(string name)
        {
            string v = (string)this.m_files.SelectToken(name);
            return v;
        }
    }
}