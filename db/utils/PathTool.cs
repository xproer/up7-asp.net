﻿using Microsoft.Experimental.IO;
using System;
using System.IO;
using System.Web;

namespace up7.db.utils
{
    public class PathTool
    {
        public static string url_decode(string v)
        {
            if (string.IsNullOrEmpty(v)) return string.Empty;
            v = v.Replace("+", "%20");
            v = HttpUtility.UrlDecode(v);//utf-8解码
            return v;
        }

        /// <summary>
        /// 自动创建多层级路径
        /// </summary>
        /// <param name="path"></param>
        /// <param name="separator">路径分隔符，默认：/</param>
        public static void createDirectory(string path, char separator = '/')
        {
            var dirs = path.Split(separator);
            var folder = "";
            foreach (var dir in dirs)
            {
                if (folder != "")
                {
                    folder = folder + "/" + dir;
                }
                else
                {
                    folder = dir;
                }
                if (!LongPathDirectory.Exists(folder))
                {
                    LongPathDirectory.Create(folder);
                }
                System.Diagnostics.Debug.WriteLine(folder);
            }
        }

        /// <summary>
        /// 合并路径，自动判断结尾符
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string combin(string a, string b)
        {
            if (a.EndsWith("/") || a.EndsWith("\\")) a = a.Remove(a.Length - 1, 1);
            if (b.StartsWith("/") || b.StartsWith("\\")) b = b.Remove(0, 1);

            return a + "/" + b;
        }

        public static string sizeFormat(double byteCount)
        {
            string[] units = new string[] { "B", "KB", "MB", "GB", "TB", "PB" };
            if (byteCount == 0)
                return "0" + units[0];

            double bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + units[place];
        }

        /// <summary>
        /// 获取路径中的文件名称（或目录名称）
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string getName(string n)
        {
            n = n.Replace('\\', '/');
            var pos = n.LastIndexOf('/');
            if (pos == -1) return n;
            return n.Substring(pos + 1);
        }

        /// <summary>
        /// 取父级目录并自动转换分隔符
        /// C:\soft\qq.exe => C:/soft
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string parentDir(string n)
        {
            return Path.GetDirectoryName(n).Replace('\\', '/');
        }

        public static void mkdirsFromFile(String f)
        {
            String dir = parentDir(f);
            PathTool.createDirectory(dir);
        }

        public static bool mkdir(string dir)
        {
            if (!Directory.Exists(dir))
            {
                var inf = Directory.CreateDirectory(dir);
                return inf.Exists;
            }
            return true;
        }

        /// <summary>
        /// 格式化文件大小
        /// </summary>
        /// <param name="byteCount"></param>
        /// <returns></returns>
        public static string BytesToString(long byteCount)
        {
            string[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (byteCount == 0)
                return "0" + suf[0];
            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return (Math.Sign(byteCount) * num).ToString() + suf[place];
        }
    }
}