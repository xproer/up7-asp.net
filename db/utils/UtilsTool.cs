﻿using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using Org.BouncyCastle.Utilities.Zlib;
using System.IO;
using System.IO.Compression;
using System.Web.Management;

namespace up7.db.utils
{
    public class UtilsTool
    {
        public static byte[] toBytes(Stream s)
        {
            s.Seek(0, SeekOrigin.Begin);
            byte[] arr = new byte[s.Length];
            s.Read(arr, 0, arr.Length);
            return arr;
        }

        /// <summary>
        /// sm4-cbc-pkcs7padding-块对齐大小
        /// </summary>
        public static int sm4AlignBlockSize(int blockSize) {
            int len = blockSize % 16;
            len = 16 - len;
            return blockSize + len;
        }

        public static Stream unCompress(Stream s, string tp)
        {
            if (tp=="zip") return unZip(s);
            return unGzip(s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="type">gzip,zip</param>
        /// <returns></returns>
        public static Stream unGzip(Stream s) { 
            s.Seek(0,SeekOrigin.Begin);
            var gz = new GZipInputStream(s);
            var ms = new MemoryStream();
            gz.CopyTo(ms);
            return ms;
        }

        public static Stream unZip(Stream s) {
            s.Seek(0, SeekOrigin.Begin);
            var z = new InflaterInputStream(s);
            var ms = new MemoryStream();
            z.CopyTo(ms);
            return ms;
        }
    }
}