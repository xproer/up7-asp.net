﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text;
using System.Web;
using up7.db.utils;

namespace up7.filemgr.app
{
    public class WebBase : System.Web.UI.Page
    {
        /// <summary>
        /// 默认加载data/config/config.json
        /// </summary>
        public JToken m_config;
        public JObject m_site;
        //
        public JToken m_cfgObj;
        public ConfigReader m_webCfg;
        /// <summary>
        /// 页面级的变量
        /// 后台：
        ///     param["id"] = 1;
        /// 定义：
        ///     param.query 查询变量
        /// 前台：
        /// <%= this.param["id"]%>
        /// </summary>
        public JObject param = new JObject();
        /// <summary>
        /// 默认加载/data/config/AppPath.json
        /// </summary>
        public JToken m_path;

        public WebBase()
        {
            this.m_webCfg = new ConfigReader();
            this.m_cfgObj = this.m_webCfg.m_files;
            this.regParamRequest();//注册请求变量
        }

        /// <summary>
        /// 获取AppPath.json中的路径
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public JToken path(string p) {
            return this.m_path.SelectToken(p);
        }

        public void regParamRequest() {
            JObject query = new JObject();
            foreach (var key in HttpContext.Current.Request.QueryString.Keys)
            {
                var kv = HttpContext.Current.Request.QueryString[key.ToString()];
                JObject obj = new JObject { { key.ToString(), kv } };
                query.Add(key.ToString(), kv);
            }

            this.param.Add("query", query);
            this.param.Add("url", HttpContext.Current.Request.Url.AbsoluteUri);
        }

        public JObject request_to_json() {
            JObject query = new JObject();
            foreach (var key in HttpContext.Current.Request.QueryString.Keys)
            {
                var kv = HttpContext.Current.Request.QueryString[key.ToString()];
                JObject obj = new JObject { { key.ToString(), kv } };
                query.Add(key.ToString(), kv);
            }
            return query;
        }

        /// <summary>
        /// 注册页面级变量
        /// </summary>
        /// <returns></returns>
        public string paramPage()
        {
            string v = string.Format("<script>var page={0};</script>", JsonConvert.SerializeObject(this.param));
            return v;
        }

        public string toInclude(string file)
        {
            bool css = file.ToLower().EndsWith("css");
            if (css)
            {
                return string.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />"
                    , file);
            }
            else
            {
                return string.Format("<script type=\"text/javascript\" src=\"{0}\" charset=\"{1}\"></script>"
                    , file
                    , "utf-8");
            }
        }

        public string require(params object[] ps)
        {
            StringBuilder sb = new StringBuilder();
            foreach (object f in ps)
            {
                //字符串
                if (Type.GetType("System.String") == f.GetType())
                {
                    sb.Append( toInclude(f.ToString()));
                }//json object
                else {
                    var t = JToken.FromObject(f);
                    //数组
                    if (t.Type == JTokenType.Array)
                    {
                        var arr = JArray.FromObject(t);
                        foreach (var a in arr)
                        {
                            sb.Append(toInclude(a.ToString()));
                        }
                    }
                    else {
                        sb.Append(toInclude(t.ToString()));
                    }
                }
            }
            return sb.ToString();
        }

        public string localFile(string file)
        {
            var ps = HttpContext.Current.Server.MapPath(file);
            return File.ReadAllText(ps);
        }

        public string localFile(JToken t)
        {
            StringBuilder sb = new StringBuilder();

            if (t.Type == JTokenType.Array)
            {
                foreach (var jt in t)
                {
                    var data = this.localFile(jt.ToString());
                    sb.Append(data);
                }
            }
            else if (t.Type == JTokenType.String)
            {
                sb.Append(this.localFile(t.ToString()));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 加载模板文件,
        /// </summary>
        /// <param name="file">模板文件相对路径,/data/tmp.html</param>
        /// <returns></returns>
        public string template(string file)
        {
            HtmlTemplater ht = new HtmlTemplater();
            ht.setFile(file);
            return ht.toString();
        }

        public string template(JToken t)
        {
            StringBuilder sb = new StringBuilder();

            HtmlTemplater ht = new HtmlTemplater();
            if (t.Type == JTokenType.Array)
            {
                foreach (var jt in t)
                {
                    ht.setFile(jt.ToString());
                    sb.Append(ht.ToString());
                }
            }
            else if (t.Type == JTokenType.String)
            {
                ht.setFile(t.ToString());
                sb.Append(ht.toString());
            }
            return sb.ToString();
        }

        public string reqStr(string name)
        {
            if (!Request.QueryString.HasKeys()) return string.Empty;
            if (string.IsNullOrEmpty(Request.QueryString[name])) return string.Empty;
            return Request.QueryString[name].Trim();
        }

        public WebBase request(string name, out long l)
        {
            string v = "0";
            if (!Request.QueryString.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.QueryString[name])) { }
            else v = Request.QueryString[name].Trim();
            l = long.Parse(v);
            return this;
        }

        public WebBase request(string name, out int l)
        {
            string v = "0";
            if (!Request.QueryString.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.QueryString[name])) { }
            else v = Request.QueryString[name].Trim();
            l = int.Parse(v);
            return this;
        }

        public WebBase request(string name, out string v)
        {
            if (!Request.QueryString.HasKeys()) v=string.Empty;
            else if (string.IsNullOrEmpty(Request.QueryString[name])) v=string.Empty;
            else v = Request.QueryString[name].Trim();
            return this;
        }

        public WebBase header(string name, out string v)
        {
            if (!Request.Headers.HasKeys()) v = string.Empty;
            else if (string.IsNullOrEmpty(Request.Headers[name])) v = string.Empty;
            else v = Request.Headers[name].Trim();
            return this;
        }

        public string headStr(string name)
        {
            if (!Request.Headers.HasKeys()) return string.Empty;
            else if (string.IsNullOrEmpty(Request.Headers[name])) return string.Empty;
            else return Request.Headers[name].Trim();
        }

        public WebBase header(string name, out bool i)
        {
            string v = "false";
            if (!Request.Headers.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.Headers[name])) { }
            else v = Request.Headers[name].Trim();
            i = Boolean.Parse(v);
            return this;
        }

        public bool headBool(string name)
        {
            string v = "false";
            if (!Request.Headers.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.Headers[name])) { }
            else v = Request.Headers[name].Trim();
            return Boolean.Parse(v);
        }

        public WebBase header(string name, out int i)
        {
            string v = "0";
            if (!Request.Headers.HasKeys()) v = string.Empty;
            else if (string.IsNullOrEmpty(Request.Headers[name])) v = string.Empty;
            else v = Request.Headers[name].Trim();
            i = int.Parse(v);
            return this;
        }

        public int headInt(string name)
        {
            string v = "0";
            if (!Request.Headers.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.Headers[name])) { }
            else v = Request.Headers[name].Trim();
            return int.Parse(v);
        }

        public WebBase header(string name, out long i)
        {
            string v = "0";
            if (!Request.Headers.HasKeys()) v = string.Empty;
            else if (string.IsNullOrEmpty(Request.Headers[name])) v = string.Empty;
            else v = Request.Headers[name].Trim();
            i = long.Parse(v);
            return this;
        }

        public long headLong(string name)
        {
            string v = "0";
            if (!Request.Headers.HasKeys()) { }
            else if (string.IsNullOrEmpty(Request.Headers[name])) { }
            else v = Request.Headers[name].Trim();
            return long.Parse(v);
        }

        public string reqStrDecode(string name)
        {
            var v = this.reqStr(name);
            return Server.UrlDecode(v);
        }

        public bool reqBool(string name)
        {
            var v = HttpContext.Current.Request.QueryString[name];
            if (string.IsNullOrEmpty(v)) return false;
            return string.Equals("true",v.Trim(),StringComparison.OrdinalIgnoreCase) || string.Equals("1",v);
        }

        public int reqInt(string name)
        {
            var v = HttpContext.Current.Request.QueryString[name];
            if (string.IsNullOrEmpty(v)) return 0;
            return int.Parse(v);
        }
        public long reqLong(string name)
        {
            var v = HttpContext.Current.Request.QueryString[name];
            if (string.IsNullOrEmpty(v)) return 0;
            return long.Parse(v);
        }

        public string headString(string name)
        {
            if (!Request.Headers.HasKeys()) return string.Empty;
            if (string.IsNullOrEmpty(Request.Headers[name])) return string.Empty;
            return Request.Headers[name].Trim();
        }

        /// <summary>
        /// 将JSON作为页面内容输出，
        /// </summary>
        /// <param name="p"></param>
        public void toContent(JToken p)
        {
            this.toContent(JsonConvert.SerializeObject(p), "application/json");
        }

        /// <summary>
        /// 将JSON作为页面内容输出，
        /// </summary>
        /// <param name="p"></param>
        public void toContent(JToken p,int code)
        {
            Response.Clear();
            Response.Write(JsonConvert.SerializeObject(p));
            Response.StatusCode = code;
            Response.End();
        }

        public void toContent(string v)
        {
            Response.Clear();
            Response.Write(v);
            Response.End();
        }

        public void toContentJson(string v)
        {
            this.toContent(v, "application/json");
        }

        public void toContent(string v, string contentType = "text/html")
        {
            Response.Clear();
            Response.AddHeader("Content-Type", contentType);
            Response.Write(v);
            Response.End();
        }

        public void toContent(string v,int code)
        {
            Response.Clear();
            Response.Write(v);
            Response.StatusCode = code;
            Response.End();
        }
        public void toContentJson(JToken t)
        {
            this.toContent(JsonConvert.SerializeObject(t), "application/json");
        }
    }
}