﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace up7.db.sql
{
    public class SqlSort
    {
        /// <summary>
        /// id desc time desc
        /// </summary>
        List<string> items = new List<string>();

        public static SqlSort build()
        {
            return new SqlSort();
        }

        public SqlSort() { }

        public SqlSort desc(string field)
        {
            this.items.Add(field + " desc");
            return this;
        }
        public SqlSort asc(string field)
        {
            this.items.Add(field + " asc");
            return this;
        }

        public string toSql()
        {
            if (this.items.Count == 0) return "";
            return " order by " + string.Join(",", this.items);
        }
    }
}