﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using up7.filemgr.app;

namespace up7.db.sql
{
    public class SqlSeter
    {
        /// <summary>
        /// 字段列表，默认判断操作=,a=@a
        /// </summary>
        List<SqlParam> m_params;
        List<string> m_sqls;

        public SqlSeter()
        {
            this.m_params = new List<SqlParam>();
            this.m_sqls = new List<string>();
        }

        public static SqlSeter build() { return new SqlSeter(); }

        /// <summary>
        /// 取SQL变量集合长度
        /// </summary>
        /// <returns></returns>
        public int size() { return this.m_params.Count; }

        public SqlSeter set(string name, string value)
        {
            this.m_params.Add(new SqlParam(name, value));
            return this;
        }
        public SqlSeter set(string name, int value)
        {
            this.m_params.Add(new SqlParam(name, value));
            return this;
        }
        public SqlSeter set(string name, long value)
        {
            this.m_params.Add(new SqlParam(name, value));
            return this;
        }
        public SqlSeter set(string name, bool value)
        {
            this.m_params.Add(new SqlParam(name, value));
            return this;
        }
        public SqlSeter sql(String s)
        {
            this.m_sqls.Add(s);
            return this;
        }

        public SqlParam[] toArray() { return this.m_params.ToArray(); }

        public void bind(SqlTable table, DbCommand cmd)
        {
            foreach (var p in this.m_params)
            {
                p.bind(cmd);
            }
        }

        public string toSql()
        {
            List<string> names = new List<string>();
            foreach (var p in this.m_params)
            {
                names.Add(p.Name + "=" + p.ParamterName);
            }
            foreach (var s in this.m_sqls)
            {
                names.Add(s);
            }
            return string.Join(",", names.ToArray());
        }
    }
}