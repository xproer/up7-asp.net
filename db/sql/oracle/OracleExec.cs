﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;
using up7.db.database;

namespace up7.filemgr.app
{
    public class OracleExec : SqlExec
    {
        public OracleExec()
        {
            //初始化变量设置器
            this.m_pvSetter = new OracleParValSetter();
            this.m_parSetter = new OracleParamSetter();
            this.m_pc = new OracleParamCreater();
            this.m_cmdRd = new OracleCmdReader();
        }

        public override JToken exec(string table, string sql, string fields, string newNames = "")
        {
            //加载结构
            this.m_table = this.table(table);
            var field_all = this.m_table.SelectToken("fields");
            var field_sel = this.from_fields(fields, field_all);
            var names = newNames.Split(',');
            if (string.IsNullOrEmpty(newNames)) names = fields.Split(',');

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);

            var r = db.ExecuteReader(cmd);

            JArray a = new JArray();

            while (r.Read())
            {
                int index = 0;
                var o = new JObject();
                foreach (var field in field_sel)
                {
                    var fd = field_sel[index];
                    var field_name = field["name"].ToString();
                    if (names.Length > 0) field_name = names[index];
                    var fd_type = fd["type"].ToString().ToLower();
                    o[field_name] = this.m_cmdRd[fd_type](r, index++);
                }
                a.Add(o);
            }
            r.Close();
            return JToken.FromObject(a);

        }

        public override void insert(string table, SqlParam[] pars)
        {
            //加载结构
            this.m_table = this.table(table);
            var field_object = this.m_table.SelectToken("fields");

            string sql = string.Format("insert into {0} ( {1} ) values( {2} )"
                , table
                , this.to_fields(pars)
                , this.to_param(pars));

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, pars, field_object);
            var id = db.ExecuteScalar(cmd);
            //return Convert.ToInt32(id);
        }

        public override JObject read(string table, string fields, SqlParam[] where)
        {
            //加载结构
            this.m_table = this.table(table);
            var field_object = this.m_table.SelectToken("fields");

            string[] arr = fields.Split(',');

            //所有字段
            if (string.Equals(fields, "*"))
            {
                var fns = from f in field_object select f["name"].ToString();
                arr = fns.ToArray();
            }//指定字段
            else
            {
                var field_list = fields.Split(',').ToList();
                List<string> fdArr = new List<string>(arr);
                var fns = from f in field_list
                          join fo in field_object
                          on f equals fo["name"].ToString()
                          select fo;
                field_object = JToken.FromObject(fns);
            }

            JObject o = null;
            string sql = string.Format("select {0} from {1} where {2}"
                , this.to_fields(field_object)
                , table
                , this.to_condition(where, "and"));

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, where);

            OracleCmdReader scr = new OracleCmdReader();
            var r = db.ExecuteReader(cmd);
            if (r.Read())
            {
                o = new JObject();
                int index = 0;
                foreach (var field in arr)
                {
                    var fd = field_object[index];
                    o[field] = scr[fd["type"].ToString()](r, index++);
                }
            }
            r.Close();
            return o;
        }

        public override void update(string table, SqlParam[] fields, SqlParam[] where, string predicate = "and")
        {
            //加载结构
            this.m_table = this.table(table);

            JObject o = new JObject();
            string sql = string.Format("update {0} set {1} where {2}"
                , table
                , this.to_condition(fields)
                , this.to_condition(where, predicate));

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, fields);
            this.to_param_db(cmd, where);
            db.ExecuteNonQuery(cmd);
        }

        public override void update(string table, SqlParam[] fields, string where)
        {
            //加载结构
            this.m_table = this.table(table);

            JObject o = new JObject();
            string sql = string.Format("update {0} set {1} where {2}"
                , table
                , this.to_condition(fields)
                , where);

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, fields);
            db.ExecuteNonQuery(cmd);
        }

        public override int count(string table, SqlParam[] where)
        {
            //加载结构
            this.m_table = this.table(table);

            JObject o = new JObject();
            string sql = string.Format("select count(*) from {0} where {1}"
                , table
                , this.to_condition(where, "and"));

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, where);
            var obj = db.ExecuteScalar(cmd);
            return Convert.ToInt32(obj);
        }

        public override int count(string table, string where)
        {
            //加载结构
            this.m_table = this.table(table);

            JObject o = new JObject();
            string sql = string.Format("select count(*) from {0} where {1}"
                , table
                , where);

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, where);
            var obj = db.ExecuteScalar(cmd);
            if (obj == DBNull.Value) return 0;
            return Convert.ToInt32(obj);
        }

        public override void exec_batch(string table, string sql, string fields, string where, JToken values)
        {
            //加载结构
            this.m_table = this.table(table);
            var field_all = this.m_table.SelectToken("fields");

            var field_sels = from f in fields.Split(',')
                             join field in field_all
                             on f.Trim() equals field["name"].ToString()
                             select field;

            var field_where_sels = from f in @where.Split(',')
                                   join field in field_all
                                   on f.Trim() equals field["name"].ToString()
                                   select field;

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, JToken.FromObject(field_sels));
            this.to_param_db(cmd, JToken.FromObject(field_where_sels));

            cmd.Connection.Open();
            cmd.Prepare();

            OracleValueSetter pvs = new OracleValueSetter();

            //设置条件
            foreach (var v in values)
            {
                //设置变量
                foreach (var w in field_sels)
                {
                    pvs[w["type"].ToString()](cmd, w, v);
                }

                //设置值
                foreach (var w in field_where_sels)
                {
                    pvs[w["type"].ToString()](cmd, w, v);
                }
                cmd.ExecuteNonQuery();
            }

            cmd.Connection.Close();
        }

        public override string to_fields(SqlParam[] ps)
        {
            var arr = from t in ps
                      select string.Format("{0}", t.Name);
            var name = string.Join(",", arr.ToArray());
            return name;
        }

        public override string to_fields(JToken o, string field = "name")
        {
            var arr = from t in o
                      select string.Format("{0}", t[field]);
            var name = string.Join(",", arr.ToArray());
            return name;
        }

        public override string to_param(SqlParam[] ps)
        {
            var names = from t in ps
                        select ":" + t.Name;
            var name = string.Join(",", names.ToArray());
            return name;
        }

        public override string to_condition(SqlParam[] ps, string pre = ",")
        {
            if (ps == null) return "1=1";
            if (ps.Length<1) return "1=1";

            var arr = from t in ps
                      select string.Format("{0}=:{0}", t.Name);
            var name = string.Join(" " + pre + " ", arr.ToArray());
            return name;
        }

        public override void to_param_db(DbCommand cmd, SqlParam[] ps, JToken field_all)
        {
            if (null == ps) return;

            var fs = from p in ps
                     join fo in field_all
                     on p.Name equals fo["name"].ToString()
                     select fo;
            var field_arr = fs.ToArray();

            var index = 0;
            foreach (var p in ps)
            {
                var fd = field_arr[index++];
                var fd_type = fd["type"].ToString().ToLower();
                this.m_parSetter[fd_type](cmd, p, fd);
            }
        }

        public override void to_param_db(DbCommand cmd, SqlParam[] ps)
        {
            if (null == ps) return;

            var field_objects = this.m_table.SelectToken("fields");
            var res = from p in ps
                      join field in field_objects
                      on p.Name equals field["name"].ToString()
                      select field;

            var field_curs = res.ToArray();

            for (int i = 0; i < ps.Length; i++)
            {
                var fd_type = field_curs[i]["type"].ToString();

                this.m_parSetter[fd_type](cmd, ps[i], field_curs[i]);
            }
        }

        public override void to_param_db(DbCommand cmd, JToken fields)
        {
            foreach (var o in fields)
            {
                var fd_type = o["type"].ToString().ToLower();

                this.m_pc[fd_type](cmd, o);
            }
        }

        public override JToken select(string table, string fields, SqlParam[] where, string sort = "")
        {
            //加载结构
            this.m_table = this.table(table);
            var fields_all = this.m_table.SelectToken("fields");

            string[] field_names = fields.Split(',');

            if (string.Equals(fields, "*"))
            {
                var fns = from f in fields_all select f["name"].ToString();
                field_names = fns.ToArray();
            }//指定了字段
            else
            {
                var field_sels = from fn in field_names.ToList()
                                 join item in fields_all
                                 on fn.Trim() equals item["name"].ToString().Trim()
                                 select item;
                fields_all = JToken.FromObject(field_sels.ToArray());
            }

            //防止字段名称冲突
            var fns_sql = from f in fields_all
                          select f["name"].ToString();
            fields = string.Join(",", fns_sql.ToArray());

            string sqlWhere = "";
            if (where.Length > 0) sqlWhere = string.Format("where {0}", this.to_condition(where, "and"));


            string sql = string.Format("select {0} from {1} {2}"
                , fields
                , table
                , sqlWhere);

            //有排序
            if (!string.IsNullOrEmpty(sort))
            {
                sql = string.Format("select {0} from {1} where {2} order by {3}"
                , fields
                , table
                , this.to_condition(where, "and")
                , sort);
            }

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);
            this.to_param_db(cmd, where);
            var r = db.ExecuteReader(cmd);

            JArray a = new JArray();

            while (r.Read())
            {
                int index = 0;
                var o = new JObject();
                foreach (var field in field_names)
                {
                    var fd = fields_all[index];
                    var fd_type = fd["type"].ToString().ToLower();
                    o[field] = this.m_cmdRd[fd_type](r, index++);
                }
                a.Add(o);
            }
            r.Close();
            return JToken.FromObject(a);
        }
    }
}