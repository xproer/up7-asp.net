﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace up7.filemgr.app
{
    /// <summary>
    /// 变量创建器，只创建变量，不赋值
    /// </summary>
    public class SqlParamCreater
    {
        /// <summary>
        /// Command变量创建器
        /// </summary>
        public delegate void dbParamSetDelegate(DbCommand cmd, JToken field);
        protected Dictionary<string, dbParamSetDelegate> m_dbParamSetter;

        public dbParamSetDelegate this[string index]
        {
            get { return this.m_dbParamSetter[index]; }
        }


        public SqlParamCreater()
        {
            //初始化mcd变量创建映射
            this.m_dbParamSetter = new Dictionary<string, dbParamSetDelegate>() {
                { "string",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.String;
                    p.Size = Convert.ToInt32(field["length"]);
                    cmd.Parameters.Add(p);
                } }
                ,{ "int",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.Int32;
                    cmd.Parameters.Add(p);
                } }
                ,{ "datetime",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.DateTime;
                    cmd.Parameters.Add(p);
                } }
                ,{ "long",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.Int64;
                    cmd.Parameters.Add(p);
                } }
                ,{ "smallint",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.Int16;
                    cmd.Parameters.Add(p);
                } }
                ,{ "tinyint",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.Byte;
                    cmd.Parameters.Add(p);
                } }
                ,{ "bool",(DbCommand cmd,JToken field)=>{
                    var p = cmd.CreateParameter();
                    p.Direction = ParameterDirection.Input;
                    p.ParameterName = "@" + field["name"].ToString();
                    p.DbType = DbType.Boolean;
                    cmd.Parameters.Add(p);
                } }
            };
        }
    }
}