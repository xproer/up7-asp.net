﻿using System.Collections.Generic;
using up7.db.database;
using up7.db.database.up7.sql;
using up7.db.model;

namespace up7.db.filemgr
{
    public class FolderScaner : biz.FolderSchemaDB
    {
        /// <summary>
        /// 是否覆盖同路径文件？
        /// </summary>
        public bool m_cover = false;

        public override void save(FileInf dir)
        {
            this.m_root = dir;
            this.m_files = new List<FileInf>();
            this.m_folders = new List<FileInf>();
            this.m_dirs = new Dictionary<string, FileInf>();

            //加载文件信息
            this.loadFiles(dir);

            //分析
            this.parseParent();
            this.parseDirs();
            this.updatePID();
            this.updatePathRel();

            //删除根目录，根目录已经添加到数据表
            this.m_folders.RemoveAt(0);

            //覆盖同路径文件=>/dir1/dir2/name.txt
            if(this.m_cover) this.cover();

            //添加目录数据
            SqlFolder.build().addBatch(this.m_folders);

            //添加文件数据
            SqlFile.build().addBatch(this.m_files);
        }

    }
}