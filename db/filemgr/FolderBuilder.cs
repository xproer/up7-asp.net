﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using up7.db.database;
using up7.db.sql;
using up7.db.utils;
using up7.down3.model;
using up7.filemgr.app;

namespace up7.filemgr.app
{
    /// <summary>
    /// 构建文件夹下载数据，
    /// </summary>
    public class FolderBuilder
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="id">文件夹ID</param>
        /// <returns></returns>
        public JToken build(string id)
        {
            var o = SqlTable.build("up7_files").readOne<DnFileInf>(SqlWhere.build().eq("f_id", id));
            //子目录
            if (o == null)
                o = SqlTable.build("up7_folders").readOne<DnFileInf>(SqlWhere.build().eq("f_id", id));

            string pathRoot = o.pathRel;
            var index = pathRoot.Length;

            //查询文件：select * from up7_files where CHARINDEX('/folder/',f_pathRel)=1
            string where = string.Format("CHARINDEX('{0}',f_pathRel)=1", pathRoot + "/");
            if (ConfigReader.dbType() == DataBaseType.Oracle)
                where = string.Format("instr(f_pathRel,'{0}')=1", pathRoot + "/");

            var files = SqlTable.build("up7_files").reads<DnFileInf>(
                SqlWhere.build()
                .sql("f_pathRel", where)
                .eq("f_fdTask", false)
                .eq("f_deleted", false)
                );
            foreach (DnFileInf f in files)
            {
                f.f_id = f.id;
                //删除父级路径，控件会自动拼接
                f.pathRel = f.pathRel.Substring(index);
            }
            return JToken.FromObject(files);            
        }

        /// <summary>
        /// 统计文件夹数量
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int count(string id)
        {
            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();
            var pathRel = this.getPathRel(id);
            //子目录：/test/child/
            //根目录：/test/
            pathRel += "/";
            string where = string.Format("CHARINDEX('{0}',f_pathRel)>0 and f_fdTask=0",pathRel);
            if(cfg.m_isOracle) where = string.Format("instr(f_pathRel,'{0}')>0 and f_fdTask=0 and f_deleted=0", pathRel);
            var c = se.count("up7_files", where);
            return c;
        }

        /// <summary>
        /// 获取目录总大小
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public long totalSize(string id)
        {
            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();
            var pathRel = this.getPathRel(id);
            //子目录：/test/child/
            //根目录：/test/
            pathRel += "/";
            string where = string.Format("CHARINDEX('{0}',f_pathRel)>0 and f_fdTask=0", pathRel);
            if(cfg.m_isOracle) where = string.Format("instr(f_pathRel,'{0}')>0 and f_fdTask=0 and f_deleted=0", pathRel);
            string sql = string.Format("select sum(f_lenLoc) from up7_files where {0}", where);
            var c = se.exec(sql);
            if (c == DBNull.Value) return 0;
            return Convert.ToInt64(c);
        }

        /// <summary>
        /// 获取文件夹的相对路径：
        /// 子目录：/test/文件夹
        /// 根目录：/test
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string getPathRel(string id)
        {
            DBConfig cfg = new DBConfig();
            SqlExec se = cfg.se();
            var fd = se.read("up7_files", "f_nameLoc,f_pathRel", new SqlParam[] { new SqlParam("f_id", id) });
            if (fd == null)
            {
                fd = se.read("up7_folders", "f_nameLoc,f_pathRel", new SqlParam[] { new SqlParam("f_id", id) });
            }
            var pathRel = fd["f_pathRel"].ToString();
            return pathRel;
        }
    }
}