﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using up7.db.database;
using up7.down3.model;

namespace up7.down3.biz
{
    public class DnFileOracle : DnFile
    {
        public override void Add(DnFileInf inf)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("insert into down3_files(");
            sql.Append(" f_id");
            sql.Append(",f_uid");
            sql.Append(",f_mac");
            sql.Append(",f_nameLoc");
            sql.Append(",f_pathLoc");
            sql.Append(",f_fileUrl");
            sql.Append(",f_lenSvr");
            sql.Append(",f_sizeLoc");
            sql.Append(",f_sizeSvr");
            sql.Append(",f_fdTask");

            sql.Append(") values(");
            sql.Append(" :f_id");
            sql.Append(",:f_uid");
            sql.Append(",:f_mac");
            sql.Append(",:f_nameLoc");
            sql.Append(",:f_pathLoc");
            sql.Append(",:f_fileUrl");
            sql.Append(",:f_lenSvr");
            sql.Append(",:f_sizeLoc");
            sql.Append(",:f_sizeSvr");
            sql.Append(",:f_fdTask");
            sql.Append(")");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql.ToString());
            db.AddString(ref cmd, ":f_id", inf.id, 32);
            db.AddInt(ref cmd, ":f_uid", inf.uid);
            db.AddString(ref cmd, ":f_mac", inf.mac,50);
            db.AddString(ref cmd, ":f_nameLoc", inf.nameLoc, 255);
            db.AddString(ref cmd, ":f_pathLoc", inf.pathLoc, 255);
            db.AddString(ref cmd, ":f_fileUrl", inf.fileUrl, 255);
            db.AddInt64(ref cmd, ":f_lenSvr", inf.lenSvr);
            db.AddString(ref cmd, ":f_sizeLoc", inf.sizeLoc, 10);
            db.AddString(ref cmd, ":f_sizeSvr", inf.sizeSvr, 10);
            db.AddBool(ref cmd, ":f_fdTask", inf.fdTask);
            db.ExecuteNonQuery(ref cmd);
        }

        public override void Delete(string fid, int uid)
        {
            string sql = "delete from down3_files where f_id=:f_id and f_uid=:f_uid";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);
            db.AddString(ref cmd, ":f_id", fid, 32);
            db.AddInt(ref cmd, ":f_uid", uid);
            db.ExecuteNonQuery(ref cmd);
        }

        public override void process(string fid, int uid, string sizeLoc, string perLoc)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("update down3_files set ");
            sb.Append(" f_sizeLoc =:sizeLoc");
            sb.Append(",f_perLoc=:f_perLoc");
            sb.Append(" where");
            sb.Append(" f_id =:f_id and f_uid=:f_uid");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());
            db.AddString(ref cmd, ":sizeLoc", sizeLoc, 10);
            db.AddString(ref cmd, ":f_perLoc", perLoc, 6);
            db.AddString(ref cmd, ":f_id", fid, 32);
            db.AddInt(ref cmd, ":f_uid", uid);
            db.ExecuteNonQuery(ref cmd);
        }

        public override void Clear()
        {
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand("TRUNCATE TABLE down3_files");
            db.ExecuteNonQuery(ref cmd);
        }

        public override string all_uncmp(int uid)
        {
            List<DnFileInf> files = new List<DnFileInf>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select ");
            sb.Append(" f_id");//0
            sb.Append(",f_nameLoc");//1
            sb.Append(",f_pathLoc");//2
            sb.Append(",f_perLoc");//3
            sb.Append(",f_sizeSvr");//4
            sb.Append(",f_fdTask");//5
            //
            sb.Append(" from down3_files");
            //
            sb.Append(" where f_uid=:f_uid ");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());
            db.AddInt(ref cmd, ":f_uid", uid);
            DbDataReader r = db.ExecuteReader(cmd);

            while (r.Read())
            {
                DnFileInf f = new DnFileInf();
                f.id = r.GetString(0);
                f.nameLoc = r.GetString(1);
                f.pathLoc = r.GetString(2);
                f.perLoc = r.GetString(3);
                f.sizeSvr = r.GetString(4);
                int fdTask = r.GetInt32(5);
                if(fdTask == 1) f.fdTask = true;
                files.Add(f);
            }
            r.Close();

            if (files.Count > 0)
            {
                return JsonConvert.SerializeObject(files);
            }
            return string.Empty;
        }

        public override string all_complete(int uid)
        {
            List<DnFileInf> fs = new List<DnFileInf>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select ");
            sb.Append(" up7_files.f_id");//0
            sb.Append(",up7_files.f_fdTask");//1
            sb.Append(",up7_files.f_nameLoc");//2
            sb.Append(",up7_files.f_sizeLoc");//3
            sb.Append(",up7_files.f_lenSvr");//4
            sb.Append(",up7_files.f_pathSvr");//5
            sb.Append(",up7_files.f_blockPath");//6
            sb.Append(",f_fileCount");//7
            sb.Append(" from up7_files ");
            sb.Append(" left join up7_folders on up7_folders.f_id=up7_files.f_id");
            //
            sb.Append(" where up7_files.f_uid=:f_uid and up7_files.f_deleted=0 and up7_files.f_complete=1 and up7_files.f_scan=1 and up7_files.f_fdChild=0");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());
            db.AddInt(ref cmd, ":f_uid", uid);
            DbDataReader r = db.ExecuteReader(cmd);

            while (r.Read())
            {
                DnFileInf f = new DnFileInf();
                f.id = Guid.NewGuid().ToString("N");
                f.f_id = r.GetString(0);
                int fdTask = r.GetInt32(1);
                if(fdTask == 1) f.fdTask = true;
                f.nameLoc = r.GetString(2);
                f.sizeLoc = "0byte";
                f.sizeSvr = r.GetString(3);
                f.lenSvr = r.GetInt64(4);
                f.pathSvr = r.GetString(5);
                f.blockPath = r.IsDBNull(6) ? string.Empty : r.GetString(6);
                f.fileCount = r.IsDBNull(7) ? 0 : r.GetInt32(7);
                fs.Add(f);
            }
            r.Close();
            if (fs.Count < 1) return string.Empty;
            return JsonConvert.SerializeObject(fs);
        }
    }
}