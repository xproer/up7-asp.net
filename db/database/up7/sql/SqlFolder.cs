﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using up7.db.model;
using up7.db.sql;
using up7.db.utils;

namespace up7.db.database.up7.sql
{
    public class SqlFolder
    {
        public static SqlFolder build()
        {
            var tp = ConfigReader.dbType();
            //if (tp == DataBaseType.Oracle) return new up6.oracle.OracleFolder();
            //if (tp == DataBaseType.Kingbase || tp == DataBaseType.ODBC) return new up6.odbc.OdbcFolder();
            //if (tp == DataBaseType.MongoDB) return new MongoFolder();
            //else 
                return new up7.sql.SqlFolder();
        }

        public SqlTable table() {
            return SqlTable.build("up7_folders");
        }

        public virtual FileInf read(string id)
        {
            return this.table().readOne<FileInf>(SqlWhere.build().eq("f_id", id));
        }

        public virtual FileInf read(string pathRel, string id, int uid)
        {
            return this.table().readOne<FileInf>("f_id,f_pid,f_pidRoot,f_pathSvr,f_pathRel",
                SqlWhere.build()
                .eq("f_pathRel", pathRel)
                .eq("f_deleted", false)
                .eq("f_uid", uid)
                .ineq("f_id", id)
                );
        }

        public virtual void del(string id, int uid)
        {
            SqlTable.build("up7_files").update(
                SqlSeter.build().set("f_deleted", true),
                SqlWhere.build()
                .eq("f_id", id)
                .eq("f_uid", uid));

            SqlTable.build("up7_folders").update(
                SqlSeter.build().set("f_deleted", true),
                SqlWhere.build()
                .eq("f_id", id)
                .eq("f_uid", uid));
        }

        static public void Clear()
        {
            string sql = "delete from up7_folders";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 获取文件夹文件数
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static int count(string id)
        {
            string sql = string.Format("select count(*) from up7_files where f_pidRoot='{0}'", id);

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);
            var obj = db.ExecuteScalar(cmd);
            return Convert.ToInt32(obj);
        }

        public virtual void add(FileInf f)
        {
            this.table().insert(f);
        }

        public virtual void add(ref FileInf f)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into up7_folders(");
            sb.Append(" f_id");
            sb.Append(",f_uid");
            sb.Append(",f_nameLoc");
            sb.Append(",f_pathLoc");
            sb.Append(",f_pathSvr");
            sb.Append(",f_pathRel");
            sb.Append(",f_fileCount");

            sb.Append(") values (");

            sb.Append(" @f_id");
            sb.Append(",@f_uid");
            sb.Append(",@f_name");
            sb.Append(",@f_pathLoc");
            sb.Append(",@f_pathSvr");
            sb.Append(",@f_pathRel");
            sb.Append(",@f_fileCount");
            sb.Append(") ;");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());

            db.AddString(ref cmd, "@f_id", f.id, 32);
            db.AddInt(ref cmd, "@f_uid", f.uid);
            db.AddString(ref cmd, "@f_name", f.nameLoc, 255);
            db.AddString(ref cmd, "@f_pathLoc", f.pathLoc, 512);
            db.AddString(ref cmd, "@f_pathSvr", f.pathSvr, 512);
            db.AddString(ref cmd, "@f_pathRel", f.pathRel, 512);
            db.AddInt(ref cmd, "@f_fileCount", f.fileCount);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="arr"></param>
        public virtual void addBatch(List<FileInf> arr)
        {
            this.table().inserts<FileInf>(arr.ToArray());
        }

        public virtual void complete(string id, int uid)
        {
            this.table().update(
                SqlSeter.build().set("f_complete", true),
                SqlWhere.build().eq("f_id", id)
                .eq("f_uid", uid)
                );
        }
    }
}