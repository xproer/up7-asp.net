﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using up7.db.model;
using up7.filemgr.app;

namespace up7.db.database.up7.oracle
{
    public class OracleFile : sql.SqlFile
    {
        public override void Clear()
        {
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand("truncate table up7_files");
            db.ExecuteNonQuery(cmd);
            cmd.CommandText = "truncate table up7_folders";
            db.ExecuteNonQuery(cmd);
        }

        public override void merged(string id)
        {
            string sql = "update up7_files set f_lenSvr=f_lenLoc,f_perSvr='100%',f_merged=1 where f_id=:id";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            db.AddString(ref cmd, ":id", id, 32);
            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 添加一条数据
        /// </summary>
        public override void add(ref FileInf f)
        {
            string sql = @"
                insert into up7_files(
                 f_id
                ,f_pid
                ,f_fdTask
                ,f_fdChild
                ,f_uid
                ,f_nameLoc
                ,f_nameSvr
                ,f_pathLoc
                ,f_pathSvr
                ,f_pathRel
                ,f_blockCount
                ,f_blockSize
                ,f_blockPath
                ,f_lenLoc
                ,f_sizeLoc
                ) values (
                 :f_id
                ,:f_pid
                ,:f_fdTask
                ,:f_fdChild
                ,:f_uid
                ,:f_nameLoc
                ,:f_nameSvr
                ,:f_pathLoc
                ,:f_pathSvr
                ,:f_pathRel
                ,:f_blockCount
                ,:f_blockSize
                ,:f_blockPath
                ,:f_lenLoc
                ,:f_sizeLoc
                ) ";

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            db.AddString(ref cmd, ":f_id", f.id, 32);
            db.AddString(ref cmd, ":f_pid", f.pid, 32);
            db.AddBool  (ref cmd, ":f_fdTask", f.fdTask);
            db.AddBool  (ref cmd, ":f_fdChild", f.fdChild);
            db.AddInt   (ref cmd, ":f_uid", f.uid);
            db.AddString(ref cmd, ":f_nameLoc", f.nameLoc, 255);
            db.AddString(ref cmd, ":f_nameSvr", f.nameSvr, 255);
            db.AddString(ref cmd, ":f_pathLoc", f.pathLoc, 512);
            db.AddString(ref cmd, ":f_pathSvr", f.pathSvr, 512);
            db.AddString(ref cmd, ":f_pathRel", f.pathRel, 512);
            db.AddInt   (ref cmd, ":f_blockCount", f.blockCount);
            db.AddInt   (ref cmd, ":f_blockSize", f.blockSize);
            db.AddString(ref cmd, ":f_blockPath", f.blockPath, 512);
            db.AddInt64 (ref cmd, ":f_lenLoc", f.lenLoc);
            db.AddString(ref cmd, ":f_sizeLoc", f.sizeLoc, 15);

            db.ExecuteNonQuery(cmd);
        }

        /// <summary>
        /// 取所有未上传完的文件和文件夹
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public override string uncmps(int uid)
        {
            List<FileInf> files = new List<FileInf>();
            string sql = @"select f_id
                            ,f_fdTask
                            ,f_nameLoc
                            ,f_pathLoc
                            ,f_pathSvr
                            ,f_blockPath
                            ,f_sizeLoc
                            ,f_perSvr 
                            from up7_files
                            where f_uid=:f_uid 
                                and f_complete=0 
                                and f_deleted=0";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);

            db.AddInInt32(cmd, ":f_uid", uid);

            var r = db.ExecuteReader(cmd);
            while (r.Read())
            {
                FileInf f = new FileInf();
                f.id = r.GetString(0);
                int fdTask = r.GetInt32(1);
                if (fdTask == 1) f.fdTask = true;
                f.nameLoc = r.GetString(2);
                f.pathLoc = r.GetString(3);
                f.pathSvr = r.GetString(4);
                f.blockPath = r.IsDBNull(5) ? string.Empty : r.GetString(5);
                f.sizeLoc = r.GetString(6);
                f.perSvr = r.GetString(7);
                files.Add(f);
            }
            r.Close();

            if (files.Count < 1) return string.Empty;
            return JsonConvert.SerializeObject(files);
        }

        public override void remove(string f_id,int uid)
        {
            string sql = "begin ";
            sql += "update up7_files set f_deleted=1 where f_id=:f_id and f_uid=:f_uid;";
            sql += "update up7_folders set f_deleted=1 where f_id=:f_id and f_uid=:f_uid;";
            sql += "end;";

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql);

            db.AddString(ref cmd, ":f_id", f_id,32);
            db.AddInt(ref cmd, ":f_uid", uid);
            db.ExecuteNonQuery(ref cmd);
        }

        public override void complete(string id)
        {
            string sql = "update up7_files set f_complete=1 where f_id=:id";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);
            db.AddString(ref cmd, ":id", id, 32);
            db.ExecuteNonQuery(cmd);
        }

        public override bool read(ref FileInf m)
        {
            bool ret = false;
            StringBuilder sql = new StringBuilder();
            sql.Append("select ");
            sql.Append(" f_pathSvr");
            sql.Append(",f_pathRel");
            sql.Append(",f_md5");
            sql.Append(",f_lenLoc");
            sql.Append(",f_sizeLoc");
            sql.Append(",f_pos");
            sql.Append(",f_blockCount");
            sql.Append(",f_blockSize");
            sql.Append(",f_blockPath");
            sql.Append(",f_lenSvr");
            sql.Append(",f_pid");
            sql.Append(",f_perSvr");
            sql.Append(",f_complete");
            sql.Append(",f_time");
            sql.Append(",f_deleted");
            sql.Append(",f_merged");
            sql.Append(",f_pidRoot");
            sql.Append(",f_fdTask");
            sql.Append(",f_fdChild");
            sql.Append(",f_uid");
            sql.Append(",f_nameLoc");
            sql.Append(",f_nameSvr");
            sql.Append(",f_pathLoc");
            sql.Append(",f_pathRel");
            sql.Append(" from up7_files where f_id=:f_id ");

            DbHelper db = new DbHelper();
            var cmd = db.GetCommand(sql.ToString());
            db.AddString(ref cmd, ":f_id", m.id, 32);
            var r = db.ExecuteReader(cmd);

            if (r.Read())
            {
                m.pathSvr    = r.IsDBNull(0) ? string.Empty : r.GetString(0);
                m.pathRel    = r.IsDBNull(1) ? string.Empty : r.GetString(1);
                m.md5        = r.IsDBNull(2) ? string.Empty : r.GetString(2);
                m.lenLoc     = r.IsDBNull(3) ? 0 : r.GetInt64(3);
                m.sizeLoc    = r.IsDBNull(4) ? string.Empty : r.GetString(4);
                m.blockCount = r.IsDBNull(6) ? 0 : r.GetInt32(6);
                m.blockSize  = r.IsDBNull(7) ? 0 : r.GetInt32(7);
                m.blockPath  = r.IsDBNull(8) ? string.Empty : r.GetString(8);
                m.lenSvr     = r.IsDBNull(9) ? 0 : r.GetInt64(9);
                m.pid        = r.IsDBNull(10) ? string.Empty : r.GetString(10);
                m.perSvr     = r.IsDBNull(11) ? string.Empty : r.GetString(11);
                int cmp      = r.GetInt32(12);
                if (cmp == 1) m.complete = true;
                m.time       = r.IsDBNull(13) ? DateTime.MinValue : r.GetDateTime(13);
                int del      = r.GetInt32(14);
                if (del == 1) m.deleted = true;
                //m.merged   = r.IsDBNull(15) ? false : r.GetBoolean(15);
                m.pidRoot    = r.IsDBNull(16) ? string.Empty : r.GetString(16);
                int fdTask   = r.GetInt32(17);
                if (fdTask == 1) m.fdTask = true;
                int fdChild  = r.GetInt32(18);
                if (fdChild == 1) m.fdChild = true;
                m.uid        = r.IsDBNull(19) ? 0 : r.GetInt32(19);
                m.nameLoc    = r.IsDBNull(20) ? string.Empty : r.GetString(20);
                m.nameSvr    = r.IsDBNull(21) ? string.Empty : r.GetString(21);
                m.pathLoc    = r.IsDBNull(22) ? string.Empty : r.GetString(22);
                m.pathRel    = r.IsDBNull(23) ? string.Empty : r.GetString(23);
                ret = true;
            }
            r.Close();
            return ret;
        }

        public override void process(string id, string perSvr)
        {
            string sql = "update up7_files set f_perSvr=:perSvr where f_id=:id";
            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sql);
            db.AddString(ref cmd, ":perSvr", perSvr, 6);
            db.AddString(ref cmd, ":id", id, 32);
            db.ExecuteNonQuery(cmd);
        }
    }
}