﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using up7.db.model;

namespace up7.db.database.up7.oracle
{
    public class OracleFolder : sql.SqlFolder
    {
        public override void add(ref FileInf f)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("insert into up7_folders(");
            sb.Append(" f_id");
            sb.Append(",f_uid");
            sb.Append(",f_nameLoc");
            sb.Append(",f_pathLoc");
            sb.Append(",f_pathSvr");
            sb.Append(",f_pathRel");
            sb.Append(",f_fileCount");

            sb.Append(") values (");

            sb.Append(" :f_id");
            sb.Append(",:f_uid");
            sb.Append(",:f_name");
            sb.Append(",:f_pathLoc");
            sb.Append(",:f_pathSvr");
            sb.Append(",:f_pathRel");
            sb.Append(",:f_fileCount");
            sb.Append(") ");

            DbHelper db = new DbHelper();
            DbCommand cmd = db.GetCommand(sb.ToString());

            db.AddString(ref cmd, ":f_id", f.id, 32);
            db.AddInt   (ref cmd, ":f_uid", f.uid);
            db.AddString(ref cmd, ":f_name", f.nameLoc, 255);
            db.AddString(ref cmd, ":f_pathLoc", f.pathLoc, 512);
            db.AddString(ref cmd, ":f_pathSvr", f.pathSvr, 512);
            db.AddString(ref cmd, ":f_pathRel", f.pathRel, 512);
            db.AddInt   (ref cmd, ":f_fileCount", f.fileCount);

            db.ExecuteNonQuery(cmd);
        }
    }
}