﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using up7.db.biz;
using up7.down3.biz;
using up7.filemgr.app;

namespace up7.db.database
{
    public class DBConfig
    {
        public bool m_isOracle = false;

        public DBConfig()
        {
            DbHelper db = new DbHelper();
            this.m_isOracle = db.isOracle();
        }

        public up7.sql.SqlFile db()
        {
            if (this.m_isOracle) return new up7.oracle.OracleFile();
            else return new up7.sql.SqlFile();
        }

        public DnFile downF()
        {
            if (this.m_isOracle) return new DnFileOracle();
            else return new DnFile();
        }

        public fd_scan sa()
        {
            if (this.m_isOracle) return new fd_scan_oracle();
            else return new fd_scan();
        }

        public fd_page fp()
        {
            if (this.m_isOracle) return new fd_page_oracle();
            else return new fd_page();
        }

        public SqlExec se()
        {
            if (this.m_isOracle) return new OracleExec();
            else return new SqlExec();
        }

        public DbBase bs()
        {
            if (this.m_isOracle) return new OracleDbBase();
            else return new DbBase();
        }

    }
}