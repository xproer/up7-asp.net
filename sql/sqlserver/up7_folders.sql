﻿USE [up7]
GO

/****** Object:  Table [dbo].[up7_folders]    Script Date: 05/09/2019 20:06:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[up7_folders](
	[f_id] [char](32) NOT NULL,
	[f_nameLoc] [varchar](100) NULL,
	[f_pid] [char](32) NULL,
	[f_uid] [int] NULL,
	[f_lenLoc] [bigint] NULL,
	[f_sizeLoc] [varchar](50) NULL,
	[f_pathLoc] [varchar](500) NULL,
	[f_pathSvr] [varchar](500) NULL,
	[f_folders] [int] NULL,
	[f_fileCount] [int] NULL,
	[f_filesComplete] [int] NULL,
	[f_complete] [bit] NULL,
	[f_deleted] [bit] NULL,
	[f_time] [datetime] NULL,
	[f_pidRoot] [char](32) NULL,
	[f_pathRel] [nvarchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件夹名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_nameLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_uid'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数字化的大小。以字节为单位。示例：1023652' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_lenLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'格式化的大小。示例：10G' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_sizeLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件夹在客户端的路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_pathLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件夹在服务端的路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_pathSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件夹数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_folders'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_fileCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已上传完的文件数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_filesComplete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已上传完毕' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_deleted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上传时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_folders', @level2type=N'COLUMN',@level2name=N'f_time'
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_nameLoc]  DEFAULT ('') FOR [f_nameLoc]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_pid]  DEFAULT ('') FOR [f_pid]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_uid]  DEFAULT ((0)) FOR [f_uid]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_lenLoc]  DEFAULT ((0)) FOR [f_lenLoc]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_sizeLoc]  DEFAULT ('') FOR [f_sizeLoc]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_pathLoc]  DEFAULT ('') FOR [f_pathLoc]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_pathSvr]  DEFAULT ('') FOR [f_pathSvr]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_folders]  DEFAULT ((0)) FOR [f_folders]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_fileCount]  DEFAULT ((0)) FOR [f_fileCount]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_filesComplete]  DEFAULT ((0)) FOR [f_filesComplete]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_complete]  DEFAULT ((0)) FOR [f_complete]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_deleted]  DEFAULT ((0)) FOR [f_deleted]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_time]  DEFAULT (getdate()) FOR [f_time]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_pidRoot]  DEFAULT ('') FOR [f_pidRoot]
GO

ALTER TABLE [dbo].[up7_folders] ADD  CONSTRAINT [DF_up7_folders_f_pathRel]  DEFAULT ('') FOR [f_pathRel]
GO


