USE [up7]
GO

/****** Object:  Table [dbo].[up7_files]    Script Date: 03/01/2023 11:47:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[up7_files](
	[f_id] [char](32) NOT NULL,
	[f_pid] [char](32) NULL,
	[f_pidRoot] [char](32) NULL,
	[f_fdTask] [bit] NULL,
	[f_fdChild] [bit] NULL,
	[f_uid] [int] NULL,
	[f_nameLoc] [varchar](255) NULL,
	[f_nameSvr] [varchar](255) NULL,
	[f_pathLoc] [varchar](512) NULL,
	[f_pathSvr] [varchar](512) NULL,
	[f_pathRel] [varchar](512) NULL,
	[f_md5] [varchar](40) NULL,
	[f_lenLoc] [bigint] NULL,
	[f_sizeLoc] [varchar](15) NULL,
	[f_pos] [bigint] NULL,
	[f_blockCount] [int] NULL,
	[f_blockSize] [int] NULL,
	[f_blockPath] [varchar](2000) NULL,
	[f_lenSvr] [bigint] NULL,
	[f_perSvr] [varchar](6) NULL,
	[f_complete] [bit] NULL,
	[f_time] [datetime] NULL,
	[f_deleted] [bit] NULL,
	[f_scan] [bit] NULL,
	[f_lenLocSec] [bigint] NULL,
	[f_encrypt] [bit] NULL,
	[f_encryptAlgo] [varchar](10) NULL,
	[f_storage] [varchar](10) NULL,
	[f_object_key] [varchar](255) NULL,
	[f_object_url] [varchar](255) NULL
 CONSTRAINT [PK_up7_files] PRIMARY KEY CLUSTERED 
(
	[f_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'表示是否是一个文件夹上传任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_fdTask'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否是文件夹中的子项' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_fdChild'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在本地电脑中的名称。例：QQ.exe ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_nameLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的名称。一般为文件MD5+扩展名。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_nameSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在本地电脑中的完整路径。
示例：D:\Soft\QQ.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_pathLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的完整路径。
示例：F:\ftp\user1\QQ2012.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_pathSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件在服务器中的相对路径。
示例：/www/web/upload/QQ2012.exe
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_pathRel'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件MD5' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_md5'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件总长度。以字节为单位
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_lenLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'格式化的文件尺寸。示例：10MB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_sizeLoc'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件续传位置。
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_pos'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件块总数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_blockCount'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'块大小' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_blockSize'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件块保存路径' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_blockPath'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已上传长度。以字节为单位。
最大值：9,223,372,036,854,775,807
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_lenSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已上传百分比。示例：10%' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_perSvr'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已上传完毕。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_complete'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件上传时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否已删除。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_deleted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文件或文件夹是否合并完成' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'up7_files', @level2type=N'COLUMN',@level2name=N'f_scan'
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_pidSign]  DEFAULT ('') FOR [f_pid]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_rootSign]  DEFAULT ('') FOR [f_pidRoot]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_fdTask]  DEFAULT ((0)) FOR [f_fdTask]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_fdChild]  DEFAULT ((0)) FOR [f_fdChild]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_uid]  DEFAULT ((0)) FOR [f_uid]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_nameLoc]  DEFAULT ('') FOR [f_nameLoc]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_nameSvr]  DEFAULT ('') FOR [f_nameSvr]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_pathLoc]  DEFAULT ('') FOR [f_pathLoc]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_pathSvr]  DEFAULT ('') FOR [f_pathSvr]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_pathRel]  DEFAULT ('') FOR [f_pathRel]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_md5]  DEFAULT ('') FOR [f_md5]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_lenLoc]  DEFAULT ((0)) FOR [f_lenLoc]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_sizeLoc]  DEFAULT ('0Bytes') FOR [f_sizeLoc]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_pos]  DEFAULT ((0)) FOR [f_pos]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_blockCount]  DEFAULT ((1)) FOR [f_blockCount]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_blockSize]  DEFAULT ((1)) FOR [f_blockSize]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_lenSvr]  DEFAULT ((0)) FOR [f_lenSvr]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_perSvr]  DEFAULT ('0%') FOR [f_perSvr]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_complete]  DEFAULT ((0)) FOR [f_complete]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_time]  DEFAULT (getdate()) FOR [f_time]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_deleted]  DEFAULT ((0)) FOR [f_deleted]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_scan]  DEFAULT ((0)) FOR [f_scan]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_lenLocSec]  DEFAULT ((0)) FOR [f_lenLocSec]
GO

ALTER TABLE [dbo].[up7_files] ADD  CONSTRAINT [DF_up7_files_f_encrypt]  DEFAULT ((0)) FOR [f_encrypt]
GO


