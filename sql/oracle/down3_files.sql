/*
	类型参考：http://www.cnblogs.com/kerrycode/p/3265120.html
*/
drop table down3_files;
CREATE TABLE down3_files
(
     f_id      char(32)       NOT NULL
    ,f_uid     number         DEFAULT '0'
    ,f_mac     varchar2(50)   DEFAULT ''
    ,f_nameLoc varchar2(255)  DEFAULT ''
    ,f_pathLoc varchar2(255)  DEFAULT ''
    ,f_fileUrl varchar2(255)  DEFAULT ''
    ,f_perLoc  varchar2(6)    DEFAULT '0%'
    ,f_lenLoc  number(19)     DEFAULT 0
    ,f_lenSvr  number(19)     DEFAULT 0
    ,f_sizeLoc varchar2(10)   DEFAULT ''
    ,f_sizeSvr varchar2(10)   DEFAULT ''
    ,f_fdTask  number(1)      DEFAULT 0
);

--创建主键
ALTER TABLE down3_files ADD CONSTRAINT PK_down3_files PRIMARY KEY(f_id);