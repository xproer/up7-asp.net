/*
	类型参考：http://www.cnblogs.com/kerrycode/p/3265120.html
	表说明：
		保存已经上传完毕的文件和文件夹
*/
drop table up7_files;
CREATE TABLE up7_files
(
	 f_id				char(32) NOT NULL		 /*文件ID，唯一。由于f_fid与oracle数据库字段有冲突，现改为f_idSign*/
	,f_pid				char(32) DEFAULT ''		 /**/
	,f_pidRoot			char(32) DEFAULT ''		 /*根级文件夹ID*/
	,f_fdTask			number(1) DEFAULT 0	 	 /*表示是否是一个文件夹上传任务。提供给 ajax_f_list.jsp使用*/
	,f_fdChild			number(1) DEFAULT 0		 /*是文件夹中的子项*/
	,f_uid  			number DEFAULT '0'   	 /*用户ID*/
	,f_nameLoc 			varchar2(255)  DEFAULT '' /*文件在本地电脑中的名称。例：QQ.exe*/ 
	,f_nameSvr  		varchar2(255)  DEFAULT '' /*文件在服务器中的名称。一般为文件MD5+扩展名。*/
	,f_pathLoc 			varchar2(512)  DEFAULT '' /*文件在本地电脑中的完整路径。*/
	,f_pathSvr 		 	varchar2(512)  DEFAULT '' /*文件在服务器中的完整路径。*/
	,f_pathRel  		varchar2(512)  DEFAULT '' /*文件在服务器中的相对路径。*/
	,f_md5  			varchar2(40)   DEFAULT '' /*文件md5,sha1,crc*/
	,f_lenLoc  			number(19) DEFAULT 0  	  /*文件总长度。以字节为单位*/
	,f_sizeLoc  		varchar2(15) DEFAULT ''   /*格式化的文件尺寸。示例：10MB*/
	,f_pos  			number(19) DEFAULT 0  	  /*文件续传位置。*/
	,f_blockCount  		number DEFAULT '1'		  /*文件块总数*/
	,f_blockSize  		number DEFAULT '0'  	  /*文件块大小*/
	,f_blockPath		varchar2(2000) DEFAULT '' /*文件块路径。*/
	,f_lenSvr			number(19) DEFAULT 0  	  /*已上传长度。以字节为单位。*/
	,f_perSvr  			varchar2(6) DEFAULT '0%'  /*已上传百分比。示例：10%*/
	,f_complete  		number(1) DEFAULT 0    	  /*是否已上传完毕。*/
	,f_time  			DATE DEFAULT sysdate      /*文件上传时间*/
	,f_deleted  		number(1) DEFAULT 0    	  /*是否已删除。*/
	,f_scan				number(1)  DEFAULT 0
	,f_lenLocSec  		number(19) DEFAULT 0
	,f_encrypt  		number(1) DEFAULT 0
	,f_storage  		varchar2(10) DEFAULT ''
	,f_object_key 		varchar2(255)  DEFAULT ''
	,f_object_url 		varchar2(255)  DEFAULT ''
);

--创建主键
ALTER TABLE up7_files ADD CONSTRAINT PK_up7_files PRIMARY KEY(f_id);