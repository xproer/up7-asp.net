﻿/*
版权所有(C) 2009-2023 荆门泽优软件有限公司
保留所有权利
产品网站：http://www.ncmem.com/webapp/down3/index.aspx
控件下载：http://www.ncmem.com/webapp/down3/pack.aspx
联系邮箱：1085617561@qq.com
联系QQ：1085617561
版本：7.2.9
更新记录：
    2022-11-18 优化
    2015-08-13 优化
	2014-02-27 优化版本号。
    2009-11-05 创建
*/
function DownloaderMgr()
{
    //根路径：http://localhost:8888/
    var root = [
        window.location.protocol,
        "//",
        window.location.host,
        "/api/down3/"
    ].join("");

    //url=>res/
    //http://localhost:8888/filemgr/res/up6/up6.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.indexOf("down3/down.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("down3/down.js"));
            }
        }
        return jsPath;
    };
    //http://localhost/res/down2/
    var pathRes = this.getJsDir() + "imgs/";

	var _this = this;
	this.Config = {
		  "Folder"		: ""
		, "Debug"		: false//调试模式
		, "LogFile"		: "f:\\log.txt"//日志文件路径。
		, "Company"		: "荆门泽优软件有限公司"
		, "Version"		: "1,2,110,51262"
		, "License2"	: ""
		, "Cookie"		: ""//
		, "ThreadCount"	: 3//并发数
		, "FilePart"	: 10485760//文件块大小，更新进度时使用，计算器：http://www.beesky.com/newsite/bit_byte.htm
		, "ThreadBlock"	: 3//文件块线程数
        , "Proxy"       : {url: ""/**http://192.168.0.1:8888 */,pwd: ""/**admin:123456 */}//代理
        //file
        , "UrlCreate"   : root + "f_create.aspx"
        , "UrlDel"      : root + "f_del.aspx"
        , "UrlList"     : root + "f_list.aspx"
        , "UrlListCmp"  : root + "f_list_cmp.aspx"
        , "UrlUpdate"   : root + "f_update.aspx"
        , "UrlDown"     : root + "f_down.aspx"
	    //folder
        , "UrlFdCreate" : root + "fd_create.aspx"
        , "UrlFdPage"   : root + "fd_page.aspx"
        //x86
        , ie: {
            part: { clsid: "57FA11EE-5E98-415C-933D-BCA188B86B5E", name: "Xproer.DownloaderPartition3" }
            , path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down3.cab"
        }
        //x64
        , ie64: {
            part: { clsid: "21B0B682-5C37-470D-8DFF-950EF93FFC08", name: "Xproer.DownloaderPartition3x64" }
            , path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down64.cab"
        }
        , firefox: { name: "", type: "application/npHttpDown3", path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down3.xpi" }
        , chrome: { name: "npHttpDown3", type: "application/npHttpDown3", path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down3.crx" }
        //Chrome 45
        , chrome45: { name: "com.xproer.down3", path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down3.crx" }
        , exe: { path: "http://res2.ncmem.com/download/down3/pack/1.0.11/down3.exe" }
        , mac: { path: "http://res2.ncmem.com/download/down3/mac/1.0.5/down3.pkg" }
        , linux: { path: "http://res2.ncmem.com/download/down3/linux/1.0.5/down3.deb" }
        , arm64: { path: "http://res2.ncmem.com/download/down3/arm64/1.0.2/com.ncmem.down3_2020.12.3-1_arm64.deb" }
        , mips64: { path: "http://res2.ncmem.com/download/down3/mips64/1.0.2/com.ncmem.down3_2020.12.3-1_mips64el.deb" }
        , edge: {protocol:"down3",port:9601,visible:false}
        , "Fields": { "uname": "test", "upass": "test", "uid": "0" }
        , errCode: {
            "0": "发送数据错误"
            , "1": "接收数据错误"
            , "2": "访问本地文件错误"
            , "3": "域名未授权"
            , "4": "文件大小超过限制"
            , "5": "地址为空"
            , "6": "配置文件不存在"
            , "7": "本地目录不存在"
            , "8": "查询文件信息失败"
            , "9": "获取文件夹分页信息失败"
            , "10": "服务器错误"
            , "11": "连接服务器失败"
        	, "12": "地址错误"
            , "13": "文件数量为0"
            , "14": "获取文件夹分页数据错误"
            , "15": "打开配置文件失败"
            , "16": "磁盘空间不足"
            , "17": "文件名称为空"
            , "18": "服务器返回大小与请求大小不一致"
            , "19": "存在同名任务"
            , "20": "路径过长"
            , "21": "访问被拒绝"
            , "22": "子文件数量超过限制"
            , "23": "连接服务器失败"
            , "24": "子文件大小超过限制"
            , "25": "返回的块大小与请求的块大小不同"
            , "26": "远程文件不存在(404)"
            , "27": "块数据不完整"
        }
        , state: {
            Ready: 0,
            Posting: 1,
            Stop: 2,
            Error: 3,
            GetNewID: 4,
            Complete: 5,
            WaitContinueUpload: 6,
            None: 7,
            Waiting: 8
        }
        , ui: {
            file: 'div[name="file"]',
            panel: 'div[name="down_panel"]',
            list: 'div[name="down_body"]',
            header: 'div[name="down_header"]',
            toolbar: 'div[name="down_toolbar"]',
            footer: 'div[name="down_footer"]',
            btn: {
            	setFolder: "span[name='btnSetFolder']",
                start: "span[name='btnStart']",
                stop: "span[name='btnStop']",
                setup: 'span[name="btnSetup"]',
                setupCmp: 'span[name="btnSetupCmp"]',
                clear: 'span[name="btnClear"]'
            },
            ele: {
                ico: {
                    file: 'img[name="file"]',
                    fd: 'img[name="folder"]'
                },
                name: 'div[name="name"]',
                size: 'div[name="size"]',
                process: 'div[name="process"]',
                percent: 'div[name="percent"]',
                msg: 'div[name="msg"]',
                btn: {
                    cancel: 'span[name="cancel"]',
                    stop: 'span[name="stop"]',
                    down: 'span[name="down"]',
                    del: 'span[name="del"]',
                    open: 'span[name="open"]',
                    openFd:'span[name="open-fd"]'
                }
            }
        }
    };

    this.event = {
        downComplete: function (obj) { },
        downError: function (obj, err) { },
        queueComplete: function () { },
        folderSel: function (path) { },
        sameFileExist: function (name) { },
        loadComplete:function(){/*控件加载完毕*/},
        unsetup:function(html){},
        ready: function () { },
        fileAppend: function (obj) { }/**添加任务 */
    };
    this.data = {
        browser: { name: navigator.userAgent.toLowerCase(), ie: true, ie64: false, firefox: false, chrome: false, edge: false, arm64: false, mips64: false }
    };
    this.ui = {
        render:null,
        file: null ,list:null,panel:null,header:null,toolbar:null,footer:null,
        btn: { render:null, selFolder: null, start: null, stop: null, setup: null, clear: null },
        ico: {
            file: pathRes + "32/file.png",
            folder: pathRes + "32/folder.png",
            open: pathRes + "32/open.png",
            folder1: pathRes + "32/folder1.png",
            stop: pathRes + "32/stop.png",
            del: pathRes + "32/del.png",
            post: pathRes + "32/post.png",
            inf:pathRes + "16/inf.png",
            postF: pathRes + "16/file.png",
            postFd: pathRes + "16/folder.png",
            paste: pathRes + "16/paste.png",
            clear: pathRes + "16/clear.png",
            config: pathRes + "16/config.png",
            "start-all": pathRes + "16/start.png",
            "stop-all": pathRes + "16/stop.png",
            ok: pathRes + "16/ok.png",
            setup: pathRes + "16/setup.png"
        }
    };
    if (arguments.length > 0) {
        var par = arguments[0];
        if (typeof (par.config) != "undefined") $.extend(true,this.Config, par.config);
        if (typeof (par.event) != "undefined") $.extend(true,this.event, par.event);
        if (typeof (par.ui) != "undefined") $.extend(true,this.ui, par.ui);
    }

    this.data.browser.ie = this.data.browser.name.indexOf("msie") > 0;
    this.data.browser.ie = this.data.browser.ie ? this.data.browser.ie : this.data.browser.name.search(/(msie\s|trident.*rv:)([\w.]+)/) != -1;
    this.data.browser.firefox = this.data.browser.name.indexOf("firefox") > 0;
    this.data.browser.chrome = this.data.browser.name.indexOf("chrome") > 0;
    this.data.browser.arm64 = this.data.browser.name.indexOf("aarch64") > 0;
    this.data.browser.mips64 = this.data.browser.name.indexOf("mips64") > 0;
    this.data.browser.edge = this.data.browser.name.indexOf("edge") > 0;
    this.pluginInited = false;
    this.edgeApp = new WebServerDown3(this);
    this.edgeApp.ent.on_close = function () { _this.socket_close(); };
    this.app = new Down3App(this);
    if (this.data.browser.edge) { this.data.browser.ie = this.data.browser.firefox = this.data.browser.chrome = this.data.browser.chrome45 = false; }
	
	this.filesMap = new Object(); //本地文件列表映射表,id,obj-json
	this.filesCmp = new Array();//已完成列表
	this.filesUrl = new Array();
    this.queueWait = new Array(); //等待队列，数据:id1,id2,id3
    this.queueWork = new Array(); //正在上传的队列，数据:id1,id2,id3
	this.parter = null;
	this.working = false;
    this.allStoped = false;//

    //api
    this.addFile = function (f) {
        if (!this.pluginCheck()) return;
        var pv = $.extend({}, f, { fileUrl: this.Config["UrlDown"] });
        this.app.addFile(pv);
    };
    this.addFolder = function (v) {
        if (!this.pluginCheck()) return;
        this.app.addFolder(v);
    };
    this.addTask = function(v){
        if(v.fdTask) this.addFolder(v);
        else this.addFile(v);
    };
    this.openConfig = function () {
        if (!this.pluginCheck()) return;
        this.app.openFolder();
    };
	this.open_folder = function (json)
	{
        if (!this.pluginCheck()) return;
	    this.app.openFolder();
	};

	this.getHtml = function()
	{ 
	    //自动安装CAB
	    var html = "";
		/*
			IE静态加载代码：
			<object id="objDownloader" classid="clsid:E94D2BA0-37F4-4978-B9B9-A4F548300E48" codebase="http://www.qq.com/HttpDownloader.cab#version=1,2,22,65068" width="1" height="1" ></object>
			<object id="objPartition" classid="clsid:6528602B-7DF7-445A-8BA0-F6F996472569" codebase="http://www.qq.com/HttpDownloader.cab#version=1,2,22,65068" width="1" height="1" ></object>
		*/
        html += '<object name="d-parter" classid="clsid:' + this.Config.ie.part.clsid + '"';
        html += ' codebase="' + this.Config.ie.part.path + '#version=' + _this.Config["Version"] + '" width="1" height="1" ></object>';
        html += '<embed name="d-ffParter" type="' + this.Config.firefox.type + '" pluginspage="' + this.Config.firefox.path + '" width="1" height="1"/>';
        return html;
	};

    this.to_params = function (param, key) {
        var paramStr = "";
        if (param instanceof String || param instanceof Number || param instanceof Boolean) {
            paramStr += "&" + key + "=" + encodeURIComponent(param);
        } else {
            $.each(param, function (i) {
                var k = key == null ? i : key + (param instanceof Array ? "[" + i + "]" : "." + i);
                paramStr += '&' + _this.to_params(this, k);
            });
        }
        return paramStr.substr(1);
    };
    this.set_config = function (v) { $.extend(this.Config, v); };
	this.clearComplete = function ()
	{
	    $.each(this.filesCmp, function (i,n)
	    {
	        n.remove();
	        _this.remove_url(n.fileSvr.nameLoc);
	    });
	    this.filesCmp.length = 0;
    };
    this.find_ui = function (o) {
        var tmp = {
            ico: {
                file:o.find(this.Config.ui.ele.ico.file)
                ,fd: o.find(this.Config.ui.ele.ico.fd)
            }
            ,name:o.find(this.Config.ui.ele.name)
            ,size:o.find(this.Config.ui.ele.size)
            , process: o.find(this.Config.ui.ele.process)
            , percent: o.find(this.Config.ui.ele.percent)
            , msg: o.find(this.Config.ui.ele.msg)
            , btn: {
                cancel:o.find(this.Config.ui.ele.btn.cancel)
                ,stop: o.find(this.Config.ui.ele.btn.stop)
                ,down: o.find(this.Config.ui.ele.btn.down)
                ,del: o.find(this.Config.ui.ele.btn.del)
                ,open: o.find(this.Config.ui.ele.btn.open)
                , openFd: o.find(this.Config.ui.ele.btn.openFd)
            }
            ,div:o
        };
        $.each(tmp.btn, function (i, n) {
            $(n).hover(function () {
                $(this).addClass("bk-hover");
            }, function () {
                $(this).removeClass("bk-hover");
            });
        });
        return tmp;
    };
    this.exist_name = function (name) {
        return this.exist_url(name);
    };
	this.add_ui = function (fileSvr)
	{
        //存在相同项
        if (this.exist_url(fileSvr.nameLoc)) { this.event.sameFileExist(fileSvr.nameLoc); return null; };
        this.filesUrl.push(fileSvr.nameLoc);

        var tmp = this.ui.file.clone();
	    tmp.css("display", "block");
        this.ui.list.append(tmp);
        var ui = this.find_ui(tmp);

	    var downer;
        if (fileSvr.fdTask) { downer = new FdDownloader(fileSvr, this); }
        else { downer = new FileDownloader(fileSvr,this);}
	    this.filesMap[fileSvr.id] = downer;//
	    downer.ui = ui;

        ui.name.text(fileSvr.nameLoc);
        ui.name.attr("title", fileSvr.fileUrl);
	    ui.msg.text("");
	    ui.size.text(fileSvr.sizeSvr);
        ui.percent.text("(" + fileSvr.perLoc + ")");
        ui.process.width(fileSvr.perLoc);

        downer.ready(); //准备
        this.event.fileAppend(downer);
        return downer;
    };
	this.resume_file = function (fSvr)
    {
        var f = $.extend({}, fSvr, { svrInit: true });
        this.add_ui(f);
    };
    this.init_file = function (f) {
        this.app.initFile(f);
    };
    this.init_folder = function (f) {
        this.app.initFolder($.extend({}, this.Config, f));
    };
    this.init_file_cmp = function (json) {
        var p = this.filesMap[json.id];
        p.init_complete(json);
    };
	this.add_file = function (f)
	{
        var obj = this.add_ui(f);
	};
    this.add_folder = function (f)
	{
        var obj = this.add_ui(f);
	};
	this.exist_url = function (url)
	{
	    var v = false;
	    for (var i = 0, l = this.filesUrl.length; i < l; ++i)
	    {
	        v = this.filesUrl[i] == url;
	        if (v) break;
	    }
	    return v;
	};
    this.remove_url = function (url) {
        this.filesUrl = $.grep(this.filesUrl, function (n, i) {
            return n == url;
        },true);
    };
	this.down_file = function (json) { };
    //队列控制
    this.work_full = function () { return (this.queueWork.length + 1) > this.Config.ThreadCount; };
    this.add_wait = function (id) { this.queueWait.push(id); };
    this.del_wait = function (id) {
        if (_this.queueWait.length < 1) return;
        this.queueWait = $.grep(this.queueWait, function (n, i) {
            return n == id;
        },true);
    };
    this.add_work = function (id) { this.queueWork.push(id); };
    this.del_work = function (id) {
        if (_this.queueWork.length < 1) return;
        this.queueWork = $.grep(this.queueWork, function (n, i) {
            return n == id;
        },true);
    };
    this.down_next = function () {
        if (_this.allStoped) return;
        if (_this.work_full()) return;
        if (_this.queueWait.length < 1) return;
        var f_id = _this.queueWait.shift();
        var f = _this.filesMap[f_id];
        _this.add_work(f_id);
        f.down();
    };

    this.init_end = function (json)
	{
	    var p = this.filesMap[json.id];
	    p.init_end(json);
	};
	this.add_end = function (json)
	{
	    var p = this.filesMap[json.id];
	    p.add_end(json);
	};
	this.down_begin = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_begin(json);
	};
	this.down_process = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_process(json);
	};
	this.down_part = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_part(json);
	};
	this.down_error = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_error(json);
    };
    this.down_open_folder = function (json) {
        //用户选择的路径
        //json.path
        this.Config["Folder"] = json.path;
        this.event.folderSel(json.path);
    };
	this.down_recv_size = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_recv_size(json);
	};
	this.down_recv_name = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_recv_name(json);
	};
	this.down_complete = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_complete(json);
	};
	this.down_stoped = function (json)
	{
        var p = this.filesMap[json.id];
	    p.down_stoped(json);
    };
    this.start_queue = function () {
    	this.allStoped = true;
        this.down_next();
    };
	this.stop_queue = function (json)
	{
		this.allStoped = true;
        $.each(this.queueWork, function (i, n) {
            _this.filesMap[n].stop();
        });
	};
	this.queue_begin = function (json) { this.working = true;};
	this.queue_end = function (json) { this.working = false;};
    this.load_complete = function (json)
    {
        this.pluginInited = true;
        
        this.ui.btn.selFolder.show();
        this.ui.btn.start.show();
        this.ui.btn.stop.show();
        this.ui.btn.clear.show();
        this.ui.list.show();
        this.ui.btn.setup.hide();
        this.ui.btn.setupCmp.hide();

        var needUpdate = true;
        if (typeof (json.version) != "undefined") {
            if (json.version == this.Config.Version) {
                needUpdate = false;
            }
        }
        if (needUpdate) this.update_notice();
        else {
            this.ui.btn.setup.hide();
            this.ui.btn.setupCmp.hide();
        }
        this.event.loadComplete();
    };
    this.load_complete_edge = function (json) {
        this.pluginInited = true;
        this.ui.btn.setup.hide();
        this.ui.btn.setupCmp.hide();
        _this.app.init();
    };
    this.socket_close = function () {
        while (_this.queueWork.length > 0)
        {
            _this.filesMap[_this.queueWork[0]].down_stoped(null);
        }
        _this.queueWork.length = 0;
        this.ui.btn.selFolder.hide();
        this.ui.btn.start.hide();
        this.ui.btn.stop.hide();
        this.ui.btn.clear.hide();
        this.ui.list.hide();
        this.ui.btn.setup.show();
        this.ui.btn.setupCmp.show();
    };
	this.recvMessage = function (str)
	{
	    var json = JSON.parse(str);
	         if (json.name == "open_files") { _this.open_files(json); }
	    else if (json.name == "init_file_cmp") { _this.init_file_cmp(json); }
	    else if (json.name == "open_folder") { _this.down_open_folder(json); }
	    else if (json.name == "down_recv_size") { _this.down_recv_size(json); }
	    else if (json.name == "down_recv_name") { _this.down_recv_name(json); }
	    else if (json.name == "init_end") { _this.init_end(json); }
	    else if (json.name == "add_file") { _this.add_file(json); }
	    else if (json.name == "add_folder") { _this.add_folder(json); }
	    else if (json.name == "add_end") { _this.add_end(json); }
	    else if (json.name == "down_begin") { _this.down_begin(json); }
	    else if (json.name == "down_process") { _this.down_process(json); }
	    else if (json.name == "down_part") { _this.down_part(json); }
	    else if (json.name == "down_error") { _this.down_error(json); }
	    else if (json.name == "down_complete") { _this.down_complete(json); }
	    else if (json.name == "down_stoped") { _this.down_stoped(json); }
	    else if (json.name == "queue_complete") { _this.event.queueComplete(); }
	    else if (json.name == "queue_begin") { _this.queue_begin(json); }
	    else if (json.name == "queue_end") { _this.queue_end(json); }
        else if (json.name == "load_complete") { _this.load_complete(json); }
	    else if (json.name == "load_complete_edge") { _this.load_complete_edge(json); }
        else if (json.name == "extension_complete") { 
            setTimeout(function () {
                var param = { name: "init", config: _this.Config };
                _this.app.postMessage(param);
            }, 1000);
        }
	};

    this.pluginLoad = function () {
        if (!this.pluginInited) {
            if (this.data.browser.edge) {
                this.edgeApp.connect();
            }
        }
    };
    this.pluginCheck = function () {
        if (!this.pluginInited) {
        	var link = "<a target='_blank' href='%url%' class='btn btn-link btn-sm'>安装控件</a>".replace("%url%",this.Config.exe.path);
            var html = '控件没有加载成功，请%link%或等待加载。'.replace("%link%",link);
            this.event.unsetup(html);
            this.pluginLoad();
            return false;
        }
        return true;
    };
	this.checkBrowser = function ()
	{
	    //Win64
	    if (window.navigator.platform == "Win64")
	    {
            $.extend(this.Config.ie, this.Config.ie64);
	    }//macOS
        else if (window.navigator.platform == "MacIntel") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.exe.path = this.Config.mac.path;
        }//linux
        else if (window.navigator.platform == "Linux x86_64") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.exe.path = this.Config.linux.path;
        }//Linux aarch64
        else if (this.data.browser.arm64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.exe.path = this.Config.arm64.path;
        }//Linux mips64
        else if (this.data.browser.mips64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.exe.path = this.Config.mips64.path;
        }
        else if (this.data.browser.firefox)
        {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
	    }
        else if (this.data.browser.chrome)
	    {
	        this.app.check = this.app.checkFF;
            $.extend(this.Config.firefox, this.Config.chrome);
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
        }
        else if (this.data.browser.edge) {
            this.app.postMessage = this.app.postMessageEdge;
        }
	};
	this.checkBrowser();

    //升级通知
    this.update_notice = function () {
    	this.ui.btn.setup.text("升级控件");
        this.ui.btn.setup.css("color", "red");
        this.ui.btn.setup.show();
    };

	//安全检查，在用户关闭网页时自动停止所有上传任务。
	this.safeCheck = function()
	{
	    $(window).bind("beforeunload", function (event)
	    {
            if (_this.queueWork.length > 0)
	        {
	            event.returnValue = "您还有程序正在运行，确定关闭？";
	        }
	    });

		$(window).bind("unload", function()
		{ 
            if (this.data.browser.edge) _this.edgeApp.close();
            if (_this.queueWork.length>0)
			{
			    _this.stop_queue();
			}
		});
    };
    this.stopAll = function () {
        $.each(this.queueWork, function (i, n) {
            _this.filesMap[n].stop_manual();
        });
        _this.queueWork.length = 0;
    };
    this.pageClose = function () {
        if (_this.queueWork.length>0) {
            _this.stopAll();
        }
        if (this.data.browser.edge) _this.edgeApp.close();
    };
	
	//加截到指定dom
	this.loadTo = function(o)
	{
		this.ui.render = o;
	    var html = this.getHtml();
	    var ui = o.append(html);
	    this.initUI(ui);
	};
	this.initUI = function (ui/*jquery obj*/)
	{
	    this.down_panel = ui.find(this.Config.ui.panel);
	    this.ui.btn.setup = ui.find(this.Config.ui.btn.setup);
        this.ui.btn.setupCmp = ui.find(this.Config.ui.btn.setupCmp);
        this.ui.file = ui.find(this.Config.ui.file);
        this.parter = ui.find('embed[name="ffParter"]').get(0);
        this.ieParter = ui.find('object[name="d-parter"]').get(0);

        this.ui.list = ui.find(this.Config.ui.list);
	    this.ui.header = ui.find(this.Config.ui.header);
	    this.ui.toolbar = ui.find(this.Config.ui.toolbar);
        this.ui.list.height(this.down_panel.parent().height());

        this.ui.btn.selFolder = ui.find(this.Config.ui.btn.setFolder);
        this.ui.btn.start = ui.find(this.Config.ui.btn.start);
        this.ui.btn.stop = ui.find(this.Config.ui.btn.stop);
        this.ui.btn.clear = ui.find(this.Config.ui.btn.clear);
        //设置图标
        $.each(this.ui.ico, function (i, n) {
            ui.find('img[name="' + i + '"]').attr("src", n);
        });

	    //设置下载文件夹
        this.ui.btn.selFolder.click(function () { _this.openConfig(); });
        this.ui.btn.setup.click(function () { window.open(_this.Config.exe.path); });
        this.ui.btn.setupCmp.click(function () { _this.edgeApp.connect(); });
        //清除已完成
        this.ui.btn.clear.click(function () { _this.clearComplete(); });
        this.ui.btn.start.click(function () { _this.start_queue(); });
        this.ui.btn.stop.click(function () { _this.stop_queue(); });
        ui.find('.toolbar-btn').hover(function () {
            $(this).addClass("bk-hover");
        }, function () { $(this).removeClass("bk-hover"); });

        this.safeCheck();//

        setTimeout(function () {
            if (!_this.data.browser.edge) {
                if (_this.data.browser.ie) {
                    _this.parter = _this.ieParter;
                }
                _this.parter.recvMessage = _this.recvMessage;
            }

            if (_this.data.browser.edge) {
                _this.edgeApp.connect();
            }
            else {
                _this.app.init();
            }
        }, 500);
	};

    //加载未未完成列表
	this.loadFiles = function ()
	{
	};
	//加载
    if (typeof (this.ui.render) == "string") {
        $(function () {
            _this.loadTo($("#" + _this.ui.render));
        });
    }
    else if (typeof (this.ui.render) == "object") {
        this.loadTo(this.ui.render);
    }
}