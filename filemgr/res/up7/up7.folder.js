﻿function FolderUploader(fdLoc, mgr)
{
    var _this = this;
    this.ui = { msg: null, process: null, percent: null, btn: { del: null, cancel: null,stop:null,post:null }, div: null};
    this.isFolder = true; //是文件夹
    this.folderInit = false;//文件夹已初始化
    this.folderScan = false;//已经扫描
    this.fileSvr = { nameLoc: "",initLoc:false,nameSvr:"",lenLoc:0,sizeLoc: "0byte", lenSvr: 0,perSvr:"0%", id: "", uid: 0, foldersCount: 0, filesCount: 0, filesComplete: 0, pathLoc: "", pathSvr: "", pathRel: "", pidRoot: 0, complete: false, folders: [], files: [] };
    $.extend(true,this.fileSvr, fdLoc);//续传信息
    this.manager = mgr;
    this.event = mgr.event;
    this.arrFiles = new Array(); //子文件列表(未上传文件列表)，存HttpUploader对象
    this.Config = mgr.Config;
    this.fields = $.extend({}, mgr.Config.Fields);//每一个对象自带一个fields幅本
    this.app = mgr.app;
    this.LocalFile = ""; //判断是否存在相同项
    this.FileName = "";

    //准备
    this.Ready = function ()
    {
        this.ui.msg.text("正在上传队列中等待...");
        this.State = this.Config.state.Ready;
        this.ui.btn.cancel.click(function () {
            _this.stop();
            _this.remove();

        });
        this.ui.btn.post.click(function () {
            _this.ui.btn.post.hide();
            _this.ui.btn.del.hide();
            _this.ui.btn.cancel.hide();
            _this.ui.btn.stop.show();

            if (!_this.manager.IsPostQueueFull()) {
                _this.post();
            }
            else {
                _this.ui.msg.text("正在上传队列中等待...");
                _this.State = _this.Config.state.Ready;
                $.each(_this.ui.btn, function (i, n) { n.hide(); });
                _this.ui.btn.del.show();
                _this.manager.AppendQueue(_this.fileSvr.id);
            }
        });
        this.ui.btn.stop.click(function () {
            _this.stop();
        });
        this.ui.btn.del.click(function () { _this.remove(); });
    };
    this.svr_create = function ()
    {
        this.ui.btn.stop.show();
        this.ui.btn.cancel.hide();
        this.ui.btn.post.hide();
        this.ui.btn.del.hide();
        this.folderInit = true;
        this.fileSvr.initLoc = true;//
        this.post_fd();
    };
    this.svr_create_err = function ()
    {
        this.folderInit = false;
        this.ui.msg.text("向服务器发送文件夹信息错误").css("cursor", "pointer").click(function ()
        {
        });
        this.ui.btn.post.show();
        this.ui.btn.stop.hide();
        this.ui.btn.del.show();
    };
    this.svr_update = function ()
    {
        var param = $.extend({}, this.fields, { uid: this.fileSvr.uid, id: this.fileSvr.id, lenSvr: this.fileSvr.lenSvr, perSvr: this.fileSvr.perSvr, time: new Date().getTime() });
        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: this.Config["UrlUpdate"]
            , data: param
            , success: function (sv) { }
            , error: function (req, txt, err) { }
            , complete: function (req, sta) { req = null; }
        });
    };
    this.svr_delete = function () {
        var param = $.extend({}, this.fields, { uid: this.fileSvr.uid, id: this.fileSvr.id, time: new Date().getTime() });
        $.ajax({
            type: "GET"
            , dataType: 'jsonp'
            , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
            , url: this.Config["UrlFdDel"]
            , data: param
            , success: function (sv) { }
            , error: function (req, txt, err) { }
            , complete: function (req, sta) { req = null; }
        });
    };
    this.scan = function ()
    {
        this.ui.btn.stop.hide();
        this.ui.btn.del.hide();
        this.ui.btn.cancel.show();
        this.app.scanFolder(this.fileSvr);
    };
    //上传，创建文件夹结构信息
    this.post = function ()
    {
        if (!this.folderScan) { this.scan(); return; }

        $.each(this.ui.btn, function (i, n) { n.hide(); });
        this.manager.AppendQueuePost(this.fileSvr.id);//添加到队列中
        this.State = this.Config.state.Posting;
        //如果文件夹已初始化，表示续传。
        if (this.folderInit)
        {
            this.post_fd();
        }
        else
        {
            if (!this.check_opened()) return;
            //在此处增加服务器验证代码。
            this.ui.msg.text("初始化...");
            var loc_path = encodeURIComponent(this.fileSvr.pathLoc);
            var f_data = $.extend({}, this.fields, {
                nameLoc: this.fileSvr.nameLoc
                , pathLoc: loc_path
                , id: this.fileSvr.id
                , lenLoc: this.fileSvr.lenLoc
                , sizeLoc: this.fileSvr.sizeLoc
                , filesCount: this.fileSvr.filesCount
                , uid: this.fileSvr.uid
                , time: new Date().getTime()
            });

            $.ajax({
                type: "GET"
                , dataType: 'jsonp'
                , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
                , url: this.Config["UrlFdCreate"]
                , data: f_data
                , success: function (msg)
                {
                    var str = decodeURIComponent(msg.value);
                    var fv = JSON.parse(str);
                    _this.fileSvr.pathSvr = fv.pathSvr;
                    _this.svr_create();
                }
                , error: function (req, txt, err)
                {
                    alert("向服务器发送文件夹信息错误！" + req.responseText);
                    _this.svr_create_err();
                }
                , complete: function (req, sta) { req = null; }

            });
            return;
        }
    };
    this.check_opened = function ()
    {
        if (this.fileSvr.files == null) return false;
        for (var i = 0, l = this.fileSvr.files.length; i < l; ++i)
        {
            var f = this.fileSvr.files[i];
            if (f.opened)
            {
                this.ui.btn.del.show();
                this.ui.btn.stop.hide();
                this.manager.RemoveQueuePost(this.fileSvr.id);//从上传队列中删除
                this.ui.msg.text("文件被占用，请关闭后重选文件夹：" + f.pathLoc);
                return false;
            }
        }
        return true;
    };
    this.check_fd = function ()
    {
        this.ui.btn.stop.show();
        this.ui.btn.post.hide();
        this.State = this.Config.state.MD5Working;
        this.app.checkFolder(this.fileSvr);
    };
    this.post_fd = function ()
    {
        this.State = this.Config.state.Posting;
        this.app.postFolder($.extend({}, this.fileSvr, { fields: this.fields }));
    };
    this.post_error = function (json)
    {
        if (json.code == "6") {
            this.ui.msg.text(this.Config.errCode[json.code] + ":" + json.pathLoc);
        }
        else
        {
            this.ui.msg.text(this.Config.errCode[json.code]);
        }

        this.ui.btn.stop.hide();
        this.ui.btn.post.show();
        this.ui.btn.del.show();
        
        this.State = this.Config.state.Error;
        //从上传列表中删除
        this.manager.RemoveQueuePost(this.fileSvr.id);
        //添加到未上传列表
        this.manager.AppendQueueWait(this.fileSvr.id);

        this.svr_update();//

        setTimeout(function () { _this.manager.PostNext(); }, 300);

        if (this.Config.AutoConnect.opened) {
            setTimeout(function () {
                if (_this.State == _this.Config.state.Posting) return;
                _this.post();
            }, this.Config.AutoConnect.time);
        }
    };
    this.post_stoped = function (json)
    {
        this.ui.msg.text("传输已停止....");
        this.ui.btn.stop.hide();
        this.ui.btn.post.show();
        this.ui.btn.del.show();

        this.State = this.Config.state.Stop;
        //从上传列表中删除
        this.manager.RemoveQueuePost(this.fileSvr.id);
        //添加到未上传列表
        this.manager.AppendQueueWait(this.fileSvr.id);
        setTimeout(function () { _this.manager.PostNext(); }, 300);
    };
    this.post_process = function (json)
    {
        this.ui.btn.stop.show();
        if (this.State == this.Config.state.Stop) return;
        this.fileSvr.lenSvr = json.lenSvr;
        this.fileSvr.perSvr = json.percent;
        this.ui.percent.text("("+json.percent+")");
        this.ui.process.css("width", json.percent);
        var str = "(" + json.fileCmps + "/" + json.fileCount + ") " + json.lenPost + " " + json.speed + " " + json.time;
        this.ui.msg.text(str);
    };
    this.post_complete = function (json)
    {
        if (!json.all)
        {
            return;
        }

        $.each(this.ui.btn, function (i, n)
        {
            n.hide();
        });
        this.ui.process.css("width", "100%");
        this.ui.percent.text("(100%)");
        //obj.pMsg.text("上传完成");
        this.State = this.Config.state.Complete;
        this.fileSvr.complete = true;
        this.fileSvr.perSvr = "100%";
        this.manager.data.cmps.push(this);
        //从上传列表中删除
        this.manager.RemoveQueuePost(this.fileSvr.id);
        //从未上传列表中删除
        this.manager.RemoveQueueWait(this.fileSvr.id);
        var str = "文件数：" + json.fileCount + "，成功：" + json.compCount;
        if (json.errorCount > 0) str += " 失败：" + json.errorCount
        this.ui.msg.text(str);

        var param = $.extend({}, this.fields, { id: this.fileSvr.id, cover:1, merge: this.Config.AutoMerge, time: new Date().getTime() });
        
        $.ajax({
            type: "GET"
			, dataType: 'jsonp'
			, jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
			, url: this.Config["UrlFdComplete"]
            , data: param
			, success: function (msg)
			{
				_this.event.fdComplete(_this);//触发事件
                _this.manager.PostNext();
			}
			, error: function (req, txt, err) { alert("向服务器发送文件夹Complete信息错误！" + req.responseText); }
			, complete: function (req, sta) { req = null; }
        });
    };
    this.md5_error = function (json)
    {
        this.ui.btn.post.show();
        this.ui.btn.cancel.hide();
    };
    this.md5_process = function (json)
    {
        if (this.State == this.Config.state.Stop) return;
        this.ui.msg.text(json.percent);
    };
    this.md5_complete = function (json)
    {
        //单个文件计算完毕
        if (!json.all)
        {
            this.fileSvr.files[json.id_f].md5 = json.md5;
            return;
        }

        //在此处增加服务器验证代码。
        this.ui.msg.text("初始化...");
        var f_data = $.extend({},this.fields,{folder: encodeURIComponent(JSON.stringify(this.fileSvr)), time: new Date().getTime()});

        $.ajax({
            type: "POST"
            //, dataType: 'jsonp'
            //, jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
			, url: this.Config["UrlFdCreate"]
			, data: f_data
			, success: function (msg)
			{
				try
				{
					var json = JSON.parse(decodeURIComponent(msg));
					_this.svr_create(json);
				}
				catch(e)
				{
					_this.post_error({"value":"7"});
				}
			}
			, error: function (req, txt, err)
			{
			    alert("向服务器发送文件夹信息错误！" + req.responseText);
			    _this.svr_create_err();
			}
			, complete: function (req, sta) { req = null; }

        });
    };

    this.scan_process = function (json)
    {
        this.ui.size.text(json.sizeLoc);
        this.ui.msg.text("已扫描:"+json.fileCount);
    };

    this.scan_complete = function (json)
    {
        this.ui.msg.text("扫描完毕，开始上传...");
        this.ui.size.text(json.sizeLoc);
        $.extend(this.fileSvr, json);
        this.folderScan = true;
        setTimeout(function () {
            _this.post();
        }, 1000);
    };
    
    //一般在StopAll()中调用
    this.stop_manual = function ()
    {
        this.svr_update();
        this.app.stopFile({ id: this.fileSvr.id,tip:false });
        this.State = this.Config.state.Stop;
    };

    //手动点击“停止”按钮时
    this.stop = function ()
    {
        this.svr_update();//
        this.ui.btn.post.hide();
        this.ui.btn.stop.hide();
        this.ui.btn.cancel.hide();
        this.State = this.Config.state.Stop;
        if (this.Config.state.Ready == this.State)
        {
            this.ui.btn.cancel.text("续传").show;
            this.ui.msg.text("传输已停止....");
            this.ui.btn.del.show();
            this.manager.RemoveQueue(this.fileSvr.id);
            this.manager.AppendQueueWait(this.fileSvr.id);//添加到未上传列表
            this.post_next();
            return;
        }
        //
        this.app.stopFile({ id: this.fileSvr.id });
        this.manager.RemoveQueuePost(this.fileSvr.id);
        this.manager.AppendQueueWait(this.fileSvr.id);
    };

    //从上传列表中删除上传任务
    this.remove = function ()
    {
        //清除缓存
        this.app.delFolder({ id: this.fileSvr.id });
        this.manager.Delete(this.fileSvr.id);
        this.ui.div.remove();
        if (this.State != this.Config.state.Complete) this.svr_delete();
    };
}